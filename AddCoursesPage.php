<?php
$subject = "";
$number = "";
$CRN = "";

if (isset($_POST["submit"])) {
    if(isset($_POST["courseSubject"])) $subject=$_POST["courseSubject"];
    if(isset($_POST["courseNum"])) $number=$_POST["courseNum"];
    if(isset($_POST["courseCRN"])) $CRN=$_POST["courseCRN"];

    //header("Location: AddCoursesPage.php");


    session_start();
    $_SESSION["UserEmail"] = "sbrown@gmail.com";
    $email = $_SESSION["UserEmail"];
    require_once("db.php");
    $sql = "SELECT * FROM user WHERE userEmail = '$email'";
    $result = $mydb->query($sql);
    $row=mysqli_fetch_array($result);
    $userID = $row['userID'];


    if(isset($_POST['courseAdd'])){
        $newCRN = $_POST['rowCRN'];
        require_once("db.php");
        $sql = "SELECT * FROM usercourse WHERE userID = $userID AND courseCRN = $newCRN";
        $result = $mydb->query($sql);
        if(mysqli_num_rows($result) == 0){
            $sql = "INSERT INTO usercourse VALUES ($userID, $newCRN)";
            $result = $mydb->query($sql);
        } else{
            $message = "You Are Already In That Course!";
            // echo "<script type='text/javascript'>alert('$message');</script>";
        }
    }
    if(isset($_POST['courseRemove'])){
        $newCRN = $_POST['rowCRN'];
        require_once("db.php");
        $sql = "DELETE FROM usercourse WHERE userID = $userID AND courseCRN = $newCRN";
        $result = $mydb->query($sql);
    }
  }
?>

<!DOCTYPE html>
<html>

    <head>
        <title>Add Courses Page</title>
        <meta name="author" content="Neha Shah">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <script src="js/bootstrap.min.js"></script>
    </head>

    <style>
        #searchInfo, #courseInfo, #userCourses{
            background: lightgrey;
        }
        #searchInfo{
            position:relative ;
            left:75px;
            bottom: 200px;
            width: 40%;
            padding:5px 35px 5px 35px;
        }
        #userCourses{
            position:relative;
            left: 600px;
            bottom: 400px;
            width: 800px;
            height: 200px;
            padding: 10px 50px 20px 50px;
            margin: 0px 50px 10px 50px;
        }
        #bioHeader{
            position:relative;
            left: 650px;
            bottom: 420px;
        }
        #courseInfo{
            position:relative;
            left: 75px;
        }
        label {
            font-size:25px;
            position: relative;
            left: 20px;
        }
        #submit{
            position: relative;
            top: 100px;
            right: 125px;
        }
        button{
            position: relative;
            bottom: 500px;
            left: 222px;
        }
        table, th, td {
          border: 1px solid black;
        }
        table {
          border-collapse: collapse;
          empty-cells: show;
          display:
        }
        th {
          color: black;
        }
        td {
        }
        #error{
            color:red;
            font-style:italic;
        }

    </style>
    <body>

      <div class="sidenav">
          <img class="logo" src="sslogo.png" width="125px">
        </div>
        <div class="content">

            <div id="searchInfo">
              <h1>Search for your Classes</h1>
                <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                  <label> Course Subject: &nbsp;&nbsp;
                    <select name="courseSubject" id="courseSubDropDown">
                      <?php
                        require_once("db.php");
                        $sql = "select courseSubject from course order by courseSubject";
                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){
                          echo "<option value='".$row["courseSubject"]."'>".$row["courseSubject"]."</option>";
                        }
                      ?>
                    </select>
                  <input type="submit" name="submit" value="Search">
                  </label><br />
                  <label> Course Number: &nbsp;&nbsp;
                    <select name="courseNum" id="courseNumDropDown">
                      <?php
                      //  require_once("db.php");
                        $sql = "select courseNum from course order by courseNum";
                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){
                          echo "<option value='".$row["courseNum"]."'>".$row["courseNum"]."</option>";
                        }
                      ?>
                    </select>
                    <input type="submit" name="submit" value="Search">
                  </label><br />
                  <h3>OR</h3>
                  <label> CRN: &nbsp;&nbsp;
                    <select name="courseCRN" id="courseCRNDropDown">
                      <?php
                        //require_once("db.php");
                        $sql = "select courseCRN from course order by courseCRN";
                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){
                          echo "<option value='".$row["courseCRN"]."'>".$row["courseCRN"]."</option>";
                        }
                      ?>
                    </select>
                    <input type="submit" name="submit" value="Search">
                  </label><br />
                  <br>
                  </form>
            </div>

          <h2 id="bioHeader">My Courses</h2>
            <div name="myCourses" id="userCourses">

                <table id="myClassInfo" border=1 width=800px>
                    <tr>
                        <th>Course Subject</th>
                        <th>Course #</th>
                        <th>CRN</th>
                        <th>Subject</th>
                        <th>Professor</th>
                        <th>Class Time</th>
                        <th></th>
                    </tr>
                    <?php
                    if (isset($_POST["submit"])) {
                      //require_once("db.php");
                      $sql = "SELECT * FROM course c, professors p WHERE c.professorID = p.professorID";
                      $result = $mydb->query($sql);
                      while($row=mysqli_fetch_array($result)){
                        echo "<tr>";
                        echo "<td>".$row['courseSubject']."</td><td> ".$row['courseNum']."</td><td>".$row['courseCRN']."</td><td>".$row['courseSubject']."</td><td>".$row['professorName']."</td><td>".$row['courseTime']."</td><td>";
                        echo "<form method='post' action='".$_SERVER['PHP_SELF']."'><input type='submit' name='courseRemove' value='Remove Course'><input type='hidden' name='rowCRN' value='".$row['courseCRN']."'></form>";
                        echo "</td></tr>";
                      }
                    }
                    ?>


                </table>


            </div>

                <table id="courseInfo" border=1 width="1500px">
                  <tr>
                      <th>Course Subject</th>
                      <th>Course #</th>
                      <th>CRN</th>
                      <th>Subject</th>
                      <th>Professor</th>
                      <th>Class Time</th>
                      <th></th>
                  </tr>

                  <?php
                  if (isset($_POST["submit"])) {
                    $id = 0;
                    if(isset($_POST['id'])) $id=$_POST['id'];
                    require_once("db.php");
                    $sql = "SELECT * FROM course c, professors p WHERE c.professorID = p.professorID and courseSubject='$subject' and courseNum='$number' or courseCRN='$CRN'"  ;
                    $result = $mydb->query($sql);
                    // $sql = "";
                    // if($id==0)
                    //   $sql ="select * from course";
                    // else {
                    //   $sql ="select * from course where courseCRN=".$id;
                    //   }
                    // $result = $mydb->query($sql);

                    // echo "<table>";
                    // echo "<thead>";
                    // echo "<th>Course Subject</th><th>Course #</th><th>CRN</th><th>Subject</th>
                    //       <th>Professor</th><th>Class Time</th>";
                    // echo "</thead>";

                      while($row = mysqli_fetch_array($result)) {
                        echo "<tr>";
                        echo "<td>".$row["courseSubject"]."</td><td>".$row["courseNum"]."</td><td>".$row["courseCRN"]."</td>
                              <td>".$row["courseSubject"]."</td><td>".$row["professorName"]."</td>
                              <td>".$row["courseTime"]."</td><td>";
                        echo "<form method='post' action='".$_SERVER['PHP_SELF']."'><input type='submit' name='courseAdd' value='Add Course'><input type='hidden' name='rowCRN' value='".$row['courseCRN']."'></form>";
                              echo "</td></tr>";
                      }
                    }
                  ?>


            </table>
        </div>
  </body>
</html>
