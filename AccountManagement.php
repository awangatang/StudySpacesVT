<?php

    session_start();
    $_SESSION["UserEmail"] = "sbrown@gmail.com";
    $email = $_SESSION["UserEmail"];

    require_once("db.php");
    $sql = "SELECT * FROM user WHERE userEmail = '$email'";
    $result = $mydb->query($sql);
    $row=mysqli_fetch_array($result);
    $userEmail = $row['userEmail'];
    $userName = $row['userFirstName']." ".$row['userLastName'];
    $userMajor = $row['userMajor'];
    $userID = $row['userID'];

    echo date('a')
?>
<!DOCTYPE html>
<html>

    <head>

        <title>Account Management Page</title>
        <meta name="author" content="Jasmine Wang">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">

    </head>

    <style>
    
        #userInfo, #classInfo, #acctBio{
            background: lightgrey;
        }

        #userInfo{
            position:relative;
            left:100px;
            top:10px;
            bottom: 500px;
            width: 25%;
            padding:25px 25px 15px 25px;
        }

        #acctBio{
            position:relative;
            left: 600px;
            bottom: 420px;
            width: 800px;
            height: 500px;
            padding: 10px 50px 20px 50px;
            margin: 0px 50px 10px 50px;
        }

        #bioHeader{
            position:relative;
            left: 650px;
            bottom: 420px;

        }

        #classInfo{
            position:relative;
            bottom: 300px;
            left: 75px;
        }

        input, button{
            font-size: 20px
        }

        label {
            font-size:25px;
        }

        #submit{
            position: relative;
            left: 125px;
        }

        button{
            position: relative;
            bottom: 500px;
            left: 222px;
        }
    
        h2{
            position: relative;
            bottom: 300px;
            left: 650px;
        }

    </style>

    <script>
    
    </script>

    <body>

        <div class="sidenav">
            <img class="logo" src="sslogo.png" width="125px" height="125px">
            <a href="Homepage.html">Home</a>
            <a href="SessionPage.html">Session</a>
            <a href="SearchPage.html">Search</a>
            <a href="AccountManagement.php">Profile</a>
        </div>
        <div class="content">

            <form id="userInfo" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
                <table>
                    <tr>
                        <td>
                        <label>Name:</label>
                        </td>
                        <td>
                        <input type="text" name="acctName" value="<?php echo $userName ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <label>Email:</label>
                        </td>
                        <td>
                        <input type="email" name="acctEmail" value="<?php echo $userEmail ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <label>Major:</label>
                        </td>
                        <td>
                        <input type="text" name="acctMajor" value="<?php echo $userMajor ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <label>Password:</label>
                        </td>
                        <td>
                        <input type="password" name="acctPW">
                        </td>
                    </tr>
                </table>

                <input type="submit" name="submit" id="submit" value="Change Info" />
             
            </form>

            <h2 id="bioHeader">About Me</h2>
            <p name="AboutMe" id="acctBio">

                <textarea rows = 30]>


                </textarea>

            </p>

            <button onclick="window.location.href = 'SessionHistory.php';">View Session History</button>

            <button onclick="window.location.href = 'SearchCoursesPage.php';">Add Classes</button>

                <h2>Your Courses</h2>
                <table id="classInfo" border=1 width="1500px">
                    <tr>
                        <th>Course #</th>
                        <th>CRN</th>
                        <th>Professor</th>
                        <th>Class Time</th>
                        <th>Course Title</th>
                    </tr>
                    <?php

                        require_once("db.php");
                        $sql = "SELECT * FROM course INNER JOIN userCourse ON course.courseCRN = userCourse.courseCRN INNER JOIN professors ON course.professorID = professors.professorID WHERE userCourse.userID = '$userID'";
                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){

                            echo "<tr>";
                            echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['courseCRN']."</td><td>".$row['professorName']."</td><td>".$row['courseDays']." ".$row['courseTime']."</td><td>";
                            echo "</tr>";

                        }

                    ?>
                </table>

        </div>
        
    </body>

</html>