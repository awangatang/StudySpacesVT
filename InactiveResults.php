<meta name="author" content="Jordan Miers" />
<?php
    session_start();
    $search = $_SESSION["sbox"];
    $_SESSION["UserEmail"] = "sbrown@gmail.com";
    $email = $_SESSION["UserEmail"];
    $filter = $_SESSION["searchType"];
    echo $filter;
    require_once("db.php");
    $sql = "SELECT * FROM user WHERE userEmail = '$email'";
    $result = $mydb->query($sql);
    $row=mysqli_fetch_array($result);
    $userID = $row['userID'];
?>

<!doctype html>
<html>

    <head>

        <title>Session History</title>
        <meta name="author" content="You">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">

    </head>

    <style>

        h1{
            text-align:center;
            vertical-align: top;
        }

        #sessionInfo{
            position: relative;
            left: 125px;
            top: 10px;
        }

    </style>

    <div class="sidenav">
      <img class="logo" src="sslogo.png" width="125px">
        <ul class="nav nav-pills">
          <li><a href="">Home</a></li>
          <li><a href="SessionPage.html">Session</a></li>
          <li class="active"><a href="SearchInactiveSessionPage.php">Search</a></li>
          <li><a href="#">Profile</a></li>
        </ul>
    </div>
    <body>

        <div class="content">

            <h1>Inactive Session Search Results</h1>

            <label>Sort By:</label>
            <select id="searchType">
                <option name = default></option>
                <option name = sortNum>Course #</option>
                <option name = sortCRN>CRN</option>
                <option name = sortProf>Professor</option>
                <option name = sortTime>Session Time</option>
            </select>

            <select id="searchDirection">
                <option name= asc>ASC</option>
                <option name=desc>DESC</option>
            </select>

            <input type="submit" name="submit" value="Search">

            </br>

            <table id="sessionInfo" border=1 width="1500px">
                    <tr>
                        <th>Course #</th>
                        <th>Session Status</th>
                        <th>Professor</th>
                        <th>Session Time</th>
                        <th>Session Info</th>
                    </tr>
                    <?php

                        require_once("db.php");

                        if($filter != "courseCRN"){

                          if ($filter != "Filters"){
                            $sql = "SELECT * FROM sessions
                                INNER JOIN usersession ON sessions.sessionID = usersession.sessionID
                                INNER JOIN course ON sessions.courseCRN = course.courseCRN
                                INNER JOIN professors ON course.professorID = professors.professorID
                                WHERE sessionStatus = 'Inactive' AND $filter LIKE '%$search%'";
                              } else {

                                $sql = "SELECT * FROM sessions
                                    INNER JOIN usersession ON sessions.sessionID = usersession.sessionID
                                    INNER JOIN course ON sessions.courseCRN = course.courseCRN
                                    INNER JOIN professors ON course.professorID = professors.professorID
                                    WHERE sessionStatus = 'Inactive'";

                              }
                          } else {

                            $sql = "SELECT * FROM sessions
                                INNER JOIN usersession ON sessions.sessionID = usersession.sessionID
                                INNER JOIN course ON sessions.courseCRN = course.courseCRN
                                INNER JOIN professors ON course.professorID = professors.professorID
                                WHERE sessionStatus = 'Inactive' AND sessions.$filter LIKE '%$search%'";

                          }

                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){

                            echo "<tr>";
                            echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['sessionStatus']."</td><td>".$row['professorName']."</td><td>".$row['sessionDate']." ".$row['sessionST']." - ".$row['sessionET']."</td><td>".$row['sessionLocation'];
                            echo "</tr>";

                        }

                    ?>
            </table>

        </div>

    </body>
</html>
