<?php
session_start();
$sessionID = $_SESSION['sessionID'];
$et = date("H:i:s");
$active = "inactive";

    require_once("db.php");
    $sql = "UPDATE sessions
            SET sessionStatus = '$active', sessionET = '$et'";
 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>End Session Page</title>
  <link rel="stylesheet" href="navigation.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="js/bootstrap.min.js"></script>


  <script src="jquery-3.1.1.min.js"></script>
  <script type="text/javascript"></script>
  <style media="screen">
    .header {
      position: absolute;
      width: 563px;
      height: 92px;
      left: 393px;
      top: 70px;

      font-family: Roboto;
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 28px;
      display: flex;
      align-items: center;
      text-align: center;

      color: #000000;
    }

    .image {
      position: absolute;
      width: 231px;
      height: 231px;
      top: 187px;
      left: 435px;
    }

    .rating {
      position: absolute;
      width: 231px;
      height: 231px;
      top: 400px;
      left: 475px;
    }

    .checked {
      color: orange;
    }
  </style>
</head>

<body>
  <div class="sidenav">
    <img class="logo" src="sslogo.png" width="125px">
    <ul class="nav nav-pills">
      <li><a href="#">Home</a></li>
      <li class="active"><a href="SessionPage.html">Session</a></li>
      <li><a href="#">Search</a></li>
      <li><a href="#">Profile</a></li>
    </ul>
  </div>
  <div class="content">
    <div class="header">Woohoo all done studying!</div>
    <div class="image">
      <img src="done_studying.svg" alt="">
    </div>
    <div class="rating">
      <h3>Rate your Session <?php echo $sessionID; ?></h3>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star"></span>
      <span class="fa fa-star"></span>

    </div>

  </div>

</body>

</html>
