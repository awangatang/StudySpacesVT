<!doctype html>
<html>
<head>
  <title>Search Results</title>
  <meta name="author" content="Edward Reed">
  <link rel="stylesheet" type="text/css" href="studyspaces.css">
</head>

 <style>
    #SessionValues, #Notes, #Comments {
      background: #C4C4C4;
      border-spacing: 10px;
    }
    .notes {
      margin-left: 800px;
      margin-top: -45px;
    }

    #Notes {
      margin-left: 800px;
      margin-top: -265px;
    }

    #Comments {
      margin-top: 220px;
    }

    .comments {
      margin-top: 100px;
    }

 </style>
<body>
  <div class="sidenav">
      <img class="logo" src="sslogo.png" width="125px" height="125px">
      <a href="Homepage.html">Home</a>
      <a href="SessionPage.html">Session</a>
      <a href="SearchPage.html">Search</a>
      <a href="AccountManagement.php">Profile</a>
  </div>
  <div class="content">
    <?php
      require_once("db.php");
      session_start();
        echo "<p>Session # ".$_SESSION["sessionID"];
        echo "<table id='SessionValues'";
        echo "<tr>";
        echo "<td>Course</td><td>BIT 4454";
        echo "</tr>";
        echo "<tr>";
        echo "<td>CRN</td><td>32254</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>Instructor</td><td>Shen</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>Time</td><td>MWF 10:30 - 11:45";
        echo "</tr>";
    ?>

  </div>

  <div class="notes">
    <p>Notes</p>
    <?php
        echo "<table id='Notes'";
        echo "<tr>";
        echo "<td>Figure out notes thing</td>"; //put in sql with notes
        echo "</tr>";
    ?>
  </div>

  <div class = "comments">
      <p>Comments</p>
      <?php
      require_once("db.php");
      $filter = $_SESSION["sessionID"];
      $sql = "select c.comment
      from comments c
      where c.sessionID = $filter";

      echo "<table id='Comments'";

      $result = $mydb->query($sql);
      while($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>".$row["comment"]."</td>"; //put in sql with notes
        echo "</tr>";
      }
      ?>
  </div>
</body>
</html>
