<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Search Professors</title>
    <meta charset="utf-8">
    <meta name="author" content="Emily Vang" />

    <link rel="stylesheet" href="studyspaces.css">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="js/bootstrap.min.js"></script>

    <script src="jquery-3.1.1.min.js"></script>
    <style media="screen">
      .search {
        background-color: lightgrey;
        position:relative;
        left:80px;
        top:5px;
        bottom: 300px;
        width: 40%;
        padding:25px 25px 15px 25px;
        border-radius: 10px;
      }
    </style>
    <script src="jquery-3.1.1.min.js"></script>
    <script>
      //ajex in Javascript
  		var asyncRequest;

      function getAllProducts() {
        //display all products
        var url = "displayProducts.php";
          try {
            asyncRequest = new XMLHttpRequest();

            asyncRequest.onreadystatechange=stateChange;
            asyncRequest.open('GET',url,true);
            asyncRequest.send();
          }
            catch (exception) {alert("Request failed");}
      }

  		function stateChange() {
  			if(asyncRequest.readyState==4 && asyncRequest.status==200) {
  				document.getElementById("tableArea").innerHTML=
  					asyncRequest.responseText;
  			}
  		}

      function clearPage(){
        document.getElementById("tableArea").innerHTML = "";
      }

      /*function init(){

        var z3 = document.getElementById("productLink");
        z3.addEventListener("mouseover", getAllProducts);

        var z4 = document.getElementById("productLink");
        z4.addEventListener("mouseout", clearPage);
      }
      document.addEventListener("DOMContentLoaded", init);*/

      //ajax in jQuery
      $(function(){
        $("#filter").change(function(){
          console.log($('#filter').val());
          $.ajax({
            url:"professors.php?filter=" + $('#filter').val(),
            async:true,
            success: function(result){
              $("#tableArea").html(result);
            }
          })
        })
      })

  	</script>
  </head>
  <body>
  <div class="sidenav">
    <img class="logo" src="sslogo.png" width="125px">
    <ul class="nav nav-pills">
      <li><a href="Homepage.php">Home</a></li>
      <li><a href="SessionPage.php">Session</a></li>
      <li class="active"><a href="SearchInactiveSessionPage.php">Search</a></li>
      <li><a href="AccountManagement.php">Profile</a></li>
    </ul>
  </div>
  <div class="content">
    <div class="search">
      <form class="searchInputs" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label>Search Professors</label>
        <br>
        <select id="filter" name="filter">
          <option value="default">Search by Subject</option>
          <?php
          require_once("db.php");
          $sql = "SELECT DISTINCT courseSubject
            FROM course
            ORDER BY courseSubject ASC";
          $result = $mydb->query($sql);
          while($row=mysqli_fetch_array($result)){
            echo "<option value='".$row["courseSubject"]."'>".$row["courseSubject"]."</option>";
          }
           ?>
        </select>
        <input type="text" name="professorName" value="Search by name">
        <input type="submit" name="submit" value="Search">

      </form>
    </div>
    <div id="tableArea">&nbsp;</div>
  </div>
  </body>
</html>
