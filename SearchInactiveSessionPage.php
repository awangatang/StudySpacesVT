<meta name="author" content="Jordan Miers" />
<?php
    session_start();
    $_SESSION["UserEmail"] = "sbrown@gmail.com";
    $email = $_SESSION["UserEmail"];

    require_once("db.php");
    $sql = "SELECT * FROM user WHERE userEmail = '$email'";
    $result = $mydb->query($sql);
    $row=mysqli_fetch_array($result);
    $userID = $row['userID'];
?>
<!DOCTYPE html>

<html>
<head>
<script type="text/javascript">

  function checkInput(){

    event.preventdefault()

}

function init(){
    var c = document.getElementById["subForm"];
    c.addEventListener("submit", checkInput, false)
}

document.addEventListener("DOMContentLoaded", init);

    </script>

</head>

    <?php
  $search="";
  $courseNum="";


  if(isset($_POST["submit"])){
    if(!empty($_POST["searchbox"])){
      session_start();
      $_SESSION["sbox"] = $_POST["searchbox"];
      $_SESSION["searchType"] = $_POST["filters"];
      Header("Location:InactiveResults.php");
    } else {
      $message = "Please Enter A Valid Search Query";
      echo "<script type = 'text/javascript'>alert('$message');</script>";
    }
  }

 ?>

    <head>

        <title>Inactive Session Page</title>
        <meta name="author" content="Jordan Miers">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">

    </head>

    <style>

        #searchInfo, #courseInfo, #userCourses{
            background: lightgrey;
        }
        #searchTitle{
          position:relative;
          top:100px;
        right:1000px;
          bottom: 500px;
          width: 25%;

        }
        #searchInfo{
            position:relative;
            left:100px;
            top:150px;
            bottom: 500px;
            width: 25%;
            padding:25px 25px 15px 25px;
        }

        #userCourses{
            position:relative;
            left: 600px;
            bottom: 200px;
            width: 1500px;
            height: 300px;
            padding: 10px 50px 20px 50px;
            margin: 0px 50px 10px 50px;
        }

        #bioHeader{
            position:relative;
            left: 650px;
            bottom: 420px;

        }

        #courseInfo{
            position:relative;
            bottom: 200px;
            left: 75px;
        }

        label {
            font-size:25px;
            position:relative;
            left: 85px;
        }

        #submit{
            position: relative;
            top: 100px;
            left: 125px;
        }

        button{
            position: relative;
            bottom: 500px;
            left: 222px;
        }

    </style>

    <body>

              <div class="sidenav">
              <img class="logo" src="sslogo.png" width="125px">
              <ul class="nav nav-pills">
                <li><a href="">Home</a></li>
                <li><a href="SessionPage.html">Session</a></li>
                <li class="active"><a href="SearchInactiveSessionPage.php">Search</a></li>
                <li><a href="#">Profile</a></li>
              </ul>
            </div>

        <div class="content">

            <div id="searchInfo">

                <h2 id="searchtitle"> Search & Filter Inactive Sessions </h2>
              </br>
              <form name="sub" id="subForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <select name="filters">
                    <option id = "default" name=default value="default">Filters</option>
                    <option name=searchCNum value="courseNum">Course Number</option>
                    <option name=searchCRN value="courseCRN">CRN</option>
                    <option name=searchProf value="professorName">Professor</option>
                </select>
                <input type="text" name= "searchbox" id="searchBox">
                <input type="submit" name="submit" value="Submit">
                  </form>

            </div>

            <h2 id="bioHeader">Search Your Inactive Results</h2>
            <div name="myCourses" id="userCourses">

              <table id="sessionInfo" border=1 width="1500px">
                     <tr>
                         <th>Course #</th>
                         <th>Session Status</th>
                         <th>Professor</th>
                         <th>Session Time</th>
                         <th>Session Info</th>
                     </tr>
                     <?php

                         require_once("db.php");
                         $sql = "SELECT * FROM sessions
                             INNER JOIN usersession ON sessions.sessionID = usersession.sessionID
                             INNER JOIN course ON sessions.courseCRN = course.courseCRN
                             INNER JOIN professors ON course.professorID = professors.professorID
                             WHERE userID = $userID";
                         $result = $mydb->query($sql);
                         while($row=mysqli_fetch_array($result)){

                             echo "<tr>";
                             echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['sessionStatus']."</td><td>".$row['professorName']."</td><td>".$row['sessionDate']." ".$row['sessionST']." - ".$row['sessionET']."</td><td>".$row['sessionLocation'];
                             echo "</tr>";

                         }
                         ?>

                        </table>
                          </div>

                      </div>

                  </body>

              </html>
