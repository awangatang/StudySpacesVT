<!doctype html>
<html>
<head>
  <title>Course Overview</title>
  <meta name="author" content="Jasmine Wang">
  <link rel="stylesheet" type="text/css" href="studyspaces.css">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>

 <style>
 
    #courseValues{
      background: #C4C4C4;
      border-spacing: 10px;
      position: relative;
      top: 200px;
    }

    #graph{
      float:right;
      margin-right:700px;
    }

    td{
      font-size:24px;
    }

    h1{
      position:relative;
      left: 600px;
      top: 100px;
    }

    h2{
      position:relative;
      top: 200px;
      left: 150px;
    }
    
    .container-fluid{
      position:relative;
      left:200px;
    }

 </style>

  <script>
        
        function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("sessionInfo");
        switching = true;
        // Set the sorting direction to ascending:
        dir = "asc";
        /* Make a loop that will continue until
        no switching has been done: */
            while (switching) {
                // Start by saying: no switching is done:
                switching = false;
                rows = table.rows;
                /* Loop through all table rows (except the
                first, which contains table headers): */
                for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /* Check if the two rows should switch place,
                based on the direction, asc or desc: */
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
                }
                if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark that a switch has been done: */
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                    // Each time a switch is done, increase this count by 1:
                    switchcount ++;
                } else {
                /* If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again. */
                    if (switchcount == 0 && dir == "asc") {
                        dir = "desc";
                        switching = true;
                    }
                }
            }
        }

    </script>

<body>
  <div class="sidenav">
      <img class="logo" src="sslogo.png" width="125px" height="125px">
      <a href="Homepage.html">Home</a>
      <a href="SessionPage.html">Session</a>
      <a href="SearchPage.html">Search</a>
      <a href="AccountManagement.php">Profile</a>
  </div>
  <div class="container-fluid">

    <h1>Course Overview</h1>

    <div id="info">
    <?php
      require_once("db.php");
      session_start();
      $crn = $_SESSION['CRNOverview'];
      $sql = "SELECT * FROM course INNER JOIN professors ON course.professorID = professors.professorID WHERE courseCRN = '$crn'";
      $result = $mydb->query($sql);
      while($row=mysqli_fetch_array($result)){
        echo  "<h2>".$row["courseSubject"]." ".$row["courseNum"]."</h2>";
        echo "<table id='courseValues'";
        echo "<tr>";
        echo "<td>Course Time:</td><td>".$row['courseDays']." ".$row['courseTime']."</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>CRN:</td><td>".$row['courseCRN']."</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>Professor:</td><td>".$row['professorName']."</td>";
        echo "</tr>";
        echo "</table>";
      }
    ?>

    </div>


  <div id="graph">


    <svg height="500" width="400"><g transform="translate(40,20)"><g class="axis axis--x" transform="translate(0,450)" fill="none" font-size="10" font-family="sans-serif" text-anchor="middle"><path class="domain" stroke="#000" d="M0.5,6V0.5H900.5V6"></path><g class="tick" opacity="1" transform="translate(225,0)"><line stroke="#000" y2="6"></line><text fill="#000" y="9" dy="0.71em">Number Of Students In The Course</text></g></g><g class="axis axis--y" fill="none" font-size="10" font-family="sans-serif" text-anchor="end"><path class="domain" stroke="#000" d="M-6,450.5H0.5V0.5H-6"></path><g class="tick" opacity="1" transform="translate(0,450.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">0</text></g><g class="tick" opacity="1" transform="translate(0,395.5)"><line stroke="#000" x2="-6"></line><text x="-9" dy="0.32em" fill="#000">2</text></g><g class="tick" opacity="1" transform="translate(0,340.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">4</text></g><g class="tick" opacity="1" transform="translate(0,286.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">6</text></g><g class="tick" opacity="1" transform="translate(0,231.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">8</text></g><g class="tick" opacity="1" transform="translate(0,176.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">10</text></g><g class="tick" opacity="1" transform="translate(0,121.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">12</text></g><g class="tick" opacity="1" transform="translate(0,67.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">14</text></g><g class="tick" opacity="1" transform="translate(0,12.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">16</text></g><text transform="rotate(-90)" y="6" dy="0.71em" text-anchor="end">Frequency</text></g><rect class="bar" x="82" y="0" width="736" height="450"></rect></g></svg>
    <script src="https://d3js.org/d3.v4.min.js"></script>
    <script>

    var svg = d3.select("svg"),
        margin = {top: 20, right: 20, bottom: 30, left: 40},
        width = +svg.attr("width") - margin.left - margin.right,
        height = +svg.attr("height") - margin.top - margin.bottom;

    var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
        y = d3.scaleLinear().rangeRound([height, 0]);
        console.log(y(3));

    var g = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //specify our data source
    /*
    d3.tsv("data.tsv", function(d) {
      d.frequency = +d.frequency;
      return d;
    }, function(error, data) {
    */

    d3.json("getData.php", function(error, data){
      if(error) throw error;

      data.forEach(function(d){
        d.letter = d.ProductName;
        d.frequency = +d.Total_InStock_Value;
      })

      console.log(data);

      if (error) throw error;

      x.domain(data.map(function(d) { return d.letter; }));
      y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

      g.append("g")
          .attr("class", "axis axis--x")
          .attr("transform", "translate(0," + height + ")")
          .call(d3.axisBottom(x));

      g.append("g")
          .attr("class", "axis axis--y")
          .call(d3.axisLeft(y).ticks(10, "s"))
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "0.71em")
          .attr("text-anchor", "end")
          .text("Frequency");

      g.selectAll(".bar")
        .data(data)
        .enter().append("rect")
          .attr("class", "bar")
          .attr("x", function(d) { return x(d.letter); })
          .attr("y", function(d) { return y(d.frequency); })
          .attr("width", x.bandwidth())
          .attr("height", function(d) { return height - y(d.frequency); });
    });

    </script>

    </div>

    <table id="sessionInfo" border=1 width="1500px">
                    <tr>
          <th onclick="sortTable(0)">Course #</th>
          <th onclick="sortTable(1)">Session Status</th>
          <th onclick="sortTable(2)">Professor</th>
          <th onclick="sortTable(3)">Session Time</th>
          <th onclick="sortTable(4)">Session Info</th>
        </tr>
        <?php

          require_once("db.php");
          $sql = "SELECT * FROM sessions
          INNER JOIN usersession ON sessions.sessionID = usersession.sessionID
          INNER JOIN course ON sessions.courseCRN = course.courseCRN
          INNER JOIN professors ON course.professorID = professors.professorID
          WHERE sessions.courseCRN = $crn";
          $result = $mydb->query($sql);
          while($row=mysqli_fetch_array($result)){

            echo "<tr>";
            echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['sessionStatus']."</td><td>".$row['professorName']."</td><td>".$row['sessionDate']." ".$row['sessionST']." - ".$row['sessionET']."</td><td>".$row['sessionLocation'];
            echo "</tr>";

          }

      ?>
    </table>

    </div>

  </body>
</html>
