<?php
$userID = 0;
$userFName = "";
$userLName = "";
$userMajor = "";
$userEmail  = "";
$userPassWord = "";
$err = false;

  if (isset($_POST["submit"])) {
    if(isset($_POST["userID"])) $userID = $_POST["userID"];
    if(isset($_POST["acctFName"])) $userFName = $_POST["acctFName"];
    if(isset($_POST["acctLName"])) $userLName = $_POST["acctLName"];
    if(isset($_POST["acctMajor"])) $userMajor = $_POST["acctMajor"];
    if(isset($_POST["acctEmail"])) $userEmail = $_POST["acctEmail"];
    if(isset($_POST["acctPW"])) $userPassWord = $_POST["acctPW"];

    require_once("db.php");

    if (!empty($userFName) && !empty($userLName)
    &&!empty($userMajor) &&!empty($userEmail) &&
    !empty($userPassWord)){
      header("HTTP/1.1 307 Temprary Redirect");
      header("Location: AddCoursesPage.php");
    }
    else {
      $err = true;
    }


  require_once("db.php");

  if ($userID == 0) {
      $sql = "INSERT into user(userID,userFirstName, userLastName, userEmail, userPassWord,userMajor)
              values('$userID','$userFName', '$userLName', '$userEmail', '$userPassWord','$userMajor')";
      $result=$mydb->query($sql);

      if ($result==1) {
        // echo "<p>A new user has been added.</p>";
      }

      // else {
      //   $err = true;
      // }


    }
    // else {
    //   $sql = "UPDATE user set userFirstName='$userFName', userLastName='$userLName', userMajor='$userMajor',
    //           userEmail='$userEmail', userPassWord='$userPassWord' where userID=$userID";
    //   $result=$mydb->query($sql);
    //
    //   if ($result==1) {
    //     echo "<p>Your account has been updated.</p>";
    //   }
    // }



}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Create Account</title>
        <meta name="author" content="Neha Shah">
        <link rel="stylesheet" type="text/css" href="studyspaceslogin.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <script src="js/bootstrap.min.js"></script>
    </head>

    <style>
      .errlabel {
        color:red;
        left:100px;
        top:10px;
        bottom: 500px;
        padding:25px 25px 15px 25px;
      }

        #userInfo, #createAcc{
            background: lightgrey;
        }

        #userInfo{
            position:relative;
            left:100px;
            top:10px;
            bottom: 500px;
            width: 40%;
            padding:25px 25px 15px 25px;
        }

        label {
            font-size:25px;
        }

        #createAcc{
            position: relative;
            left: 125px;
        }

        button{
            position: relative;
            bottom: 500px;
            left: 222px;
        }
    </style>

    <script>
    </script>

    <body>
      <div class="sidenav">
          <img class="logo" src="sslogo.png" width="125px">
        </div>

      <div class="content">
          <h1>
            Ready to study?
          </h1>
      </div>


      <form id="userInfo" class="content" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label>First Name:
          <input type="text" name="acctFName" value="<?php echo $userFName; ?>" />
          <?php
            if ($err && empty($userFName)) {
              echo "<label class='errlabel'>Error: Please enter a first name.</label>";
            }
          ?>
        </label>
        <br />

        <label>Last Name:
          <input type="text" name="acctLName" value="<?php echo $userLName; ?>" />
          <?php
            if ($err && empty($userLName)) {
              echo "<label class='errlabel'>Error: Please enter a last name.</label>";
            }
          ?>
        </label>
        <br />

        <label>Major:
          <input type="text" name="acctMajor" value="<?php echo $userMajor; ?>" />
          <?php
            if ($err && empty($userMajor)) {
              echo "<label class='errlabel'>Error: Please enter a major.</label>";
            }
          ?>
        </label>
        <br />

        <label>Email:
          <input type="email" name="acctEmail" value="<?php echo $userEmail; ?>" />
          <?php
            if ($err && empty($userEmail)) {
              echo "<label class='errlabel'>Error: Please enter an email.</label>";
            }
          ?>
        </label>
        <br />

        <label>Password:
          <input type="password" name="acctPW" />
          <?php
            if ($err && empty($userPassWord)) {
              echo "<label class='errlabel'>Error: Please enter a password.</label>";
            }
          ?>
        </label>
        <br />

        <input type="submit" name="submit" id="createAcc" value="Create Account" />
        <p>
          Already a memeber? <a href="LogIn.php">Sign In Here</a>
        </p>

 </form>


    </body>

</html>
