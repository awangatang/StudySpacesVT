<?php
    $subject = "";
    $number = "";
    $CRN = "";

    session_start();
    $_SESSION["UserEmail"] = "sbrown@gmail.com";
    $email = $_SESSION["UserEmail"];
    require_once("db.php");
    $sql = "SELECT * FROM user WHERE userEmail = '$email'";
    $result = $mydb->query($sql);
    $row=mysqli_fetch_array($result);
    $userID = $row['userID'];

    if (isset($_POST['submit'])) {
        if(isset($_POST["courseSubject"])) $subject=$_POST["courseSubject"];
    }

    if(isset($_POST['courseAdd'])){
        $newCRN = $_POST['rowCRN'];
        require_once("db.php");
        $sql = "SELECT * FROM usercourse WHERE userID = $userID AND courseCRN = $newCRN";
        $result = $mydb->query($sql);
        if(mysqli_num_rows($result) == 0){
            $sql = "INSERT INTO usercourse VALUES ($userID, $newCRN)";
            $result = $mydb->query($sql);
        } else{
            $message = "You Are Already In That Course!";
            echo "<script type='text/javascript'>alert('$message');</script>";
        }
    }

    if(isset($_POST['courseRemove'])){
        $newCRN = $_POST['rowCRN'];
        require_once("db.php");
        $sql = "DELETE FROM usercourse WHERE userID = $userID AND courseCRN = $newCRN";
        $result = $mydb->query($sql);
    }
?>

<!DOCTYPE html>
<html>

    <head>

        <title>Add Courses Page</title>
        <meta name="author" content="Neha Shah">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">

    </head>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <style>
        #searchInfo, #courseInfo, #userCourses{
            background: lightgrey;
        }
        #searchInfo{
            position:relative ;
            left:7px;
            top:20px;
            bottom: 200px;
            width: 35%;
            padding:5px 15px 5px 15px;
        }
        #userCourses{
            position:relative;
            left: 600px;
            top:500;
            bottom: 200px;
            width: 850px;
            height: 280px;
            padding:5px 15px 5px 15px;
            margin: 0px 50px 10px 50px;
        }
        #bioHeader{
            position:relative;
            left: 650px;
            bottom: 420px;
        }
        #courseInfo{
            position:relative;
            top:600;
            bottom: 100px;
            left: 200px;
            width: 1305px;
        }
        #link{
            position:relative;
            left: 200px;
            top: 10px;
            bottom: 500px;
        }
        label {
            font-size:25px;
            position: relative;
            left: 20px;
        }
        #submit{
            position: relative;
            top: 100px;
            right: 125px;
        }
        button{
            position: relative;
            bottom: 500px;
            left: 222px;
        }
        table, th, td {
          border: 1px solid black;
        }
        table {
          border-collapse: collapse;
          empty-cells: show;
          display:
        }
        th {
          color: black;
        }
        td {
        }

    </style>

    <body>

      <div class="sidenav">
          <img class="logo" src="sslogo.png" width="125px">
      </div>

        <div class="content">
            <div id="searchInfo">
              <h1>Search For Your Classes</h1>
                <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                  <label> Course Subject: &nbsp;&nbsp;
                    <select name="courseSubject" id="courseSubDropDown">
                      <?php
                        require_once("db.php");
                        $sql = "select courseSubject from course order by courseSubject";
                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){
                          echo "<option value='".$row["courseSubject"]."'>".$row["courseSubject"]."</option>";
                        }
                      ?>
                    </select>
                  <input type="submit" name="submit" value="Search">
                  </label><br />
                  </form>
            </div>
          </div>

            <div name="myCourses" id="userCourses">
                <table id="myClassInfo" border=1 width=800px><h1>My Courses</h1>
                  <tr>
                      <th>Course Subject</th>
                      <th>Course #</th>
                      <th>CRN</th>
                      <th>Professor</th>
                      <th>Class Time</th>
                      <th></th>
                  </tr>
                    <?php
                    if (isset($_POST["submit"])) {
                        require_once("db.php");
                        $sql = "SELECT * FROM course INNER JOIN userCourse ON course.courseCRN = userCourse.courseCRN INNER JOIN professors ON course.professorID = professors.professorID WHERE userCourse.userID = '$userID'";
                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){
                            echo "<tr>";
                            echo "<td>".$row["courseSubject"]."</td><td>".$row["courseNum"]."</td><td>".$row["courseCRN"]."</td>
                                  <td>".$row["professorName"]."</td>
                                  <td>".$row["courseTime"]."</td><td>";
                            echo "<form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=courseRemove value='Remove Course'><input type='hidden' name='rowCRN' value=".$row['courseCRN']."></form>";
                            echo "</tr>";
                        }
                      }
                    ?>
                </table>
            </div>

            <div>
              <table id="courseInfo" border=1 width="1500px">
                  <tr>
                      <th>Course Subject</th>
                      <th>Course #</th>
                      <th>CRN</th>
                      <th>Professor</th>
                      <th>Class Time</th>
                      <th></th>
                  </tr>
                    <?php

                        if(!isset($_GET['submit'])){
                            require_once("db.php");
                            $sql = "SELECT * FROM course c, professors p WHERE c.professorID = p.professorID AND courseSubject='$subject'";
                            //$sql = "SELECT * FROM course INNER JOIN professors ON course.professorID = professors.professorID ";
                            $result = $mydb->query($sql);
                            while($row=mysqli_fetch_array($result)){
                                echo "<tr>";
                                echo "<td>".$row["courseSubject"]."</td><td>".$row["courseNum"]."</td><td>".$row["courseCRN"]."</td>
                                      <td>".$row["professorName"]."</td>
                                      <td>".$row["courseTime"]."</td><td>";
                                echo  "<form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=courseAdd value='Add Course'><input type='hidden' name='rowCRN' value=".$row['courseCRN']."></form>";
                                echo "</tr>";
                            }
                        }
                    ?>
                </table>
        </div>

        <p id="link">
          <a href="Homepage.php">Now let's go to the Homepage!</a>
        </p>

    </body>
</html>
