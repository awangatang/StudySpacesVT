<?php
    session_start();
    $userEmail = "";
    //$userPass = "";

    $email = $_SESSION["UserEmail"];
    require_once("db.php");
    $sql = "SELECT * FROM user WHERE userEmail = '$email'";
    $result = $mydb->query($sql);
    $row=mysqli_fetch_array($result);
    $userID = $row['userID'];


    if(isset($_POST['sessionJoin'])){
        $newCRN = $_POST['rowCRN'];
        require_once("db.php");
        $sql = "SELECT * FROM usercourse WHERE userID = $userID AND courseCRN = $newCRN";
        $result = $mydb->query($sql);
        if(mysqli_num_rows($result) == 0){
            $sql = "INSERT INTO usercourse VALUES ($userID, $newCRN)";
            $result = $mydb->query($sql);
        } else{
            $message = "You Are Already In That Course!";
            Header("Location: openSessionClock.php");
            echo "<script type='text/javascript'>alert('$message');</script>";
        }
    }

    //require_once(db.php);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Homepage</title>
        <meta name="author" content="Neha Shah">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <script src="js/bootstrap.min.js"></script>
    </head>

    <style>
        #searchInfo, #courseInfo, #userCourses{
            background: lightgrey;
        }
        #userCourses{
            position:relative;
            left: 20px;
            bottom: 200px;
            width: 800px;
            height: 400px;
            padding: 10px 50px 20px 50px;
            margin: 0px 50px 10px 50px;
        }
        #bioHeader{
            position:relative;
            left: 650px;
            bottom: 420px;
        }
        #courseInfo{
            position:relative;
            bottom: 200px;
            left: 75px;
        }
        label {
            font-size:25px;
            position:relative;
            left: 85px;
        }
        button{
            position: relative;
            bottom: 500px;
            left: 222px;
        }
        td{
            text-align: center;
        }

    </style>

    <body>
      <div class="sidenav">
        <img class="logo" src="sslogo.png" width="125px">
        <ul class="nav nav-pills">
          <li class="active"><a href="Homepage.php">Home</a></li>
          <li ><a href="SessionPage.php">Session</a></li>
          <li><a href="SearchInactiveSessionPage.php">Search</a></li>
          <li><a href="AccountManagement.php">Profile</a></li>
        </ul>
      </div>

      <div class="content">
      <h2 id="bioHeader">My Courses</h2>
      <div name="myCourses" id="userCourses">

        <h1>Welcome!</h1>
        <h3>These are your current active sessions for your courses</h3>
        <br>
        <table border="1" width=600px>
          <th>Course</th>
          <th>CRN</th>
          <th>Professor</th>
          <th>Class Time</th>
          <th>Location</th>
          <th></th>

          <?php
          require_once("db.php");
          $sql = "SELECT * FROM sessions
                  INNER JOIN userCourse ON sessions.courseCRN = userCourse.courseCRN
                  INNER JOIN course ON userCourse.courseCRN = course.courseCRN
                  INNER JOIN professors ON course.professorID = professors.professorID
                  WHERE userCourse.userID = $userID AND sessions.sessionStatus = 'active'";
          $result = $mydb->query($sql);
          while($row=mysqli_fetch_array($result)){
              echo "<tr>";
              echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['courseCRN']."</td><td>".$row['professorName']."</td> <td>".$row['courseDays']." ".$row['courseTime']."</td><td>".$row['sessionLocation']."</td><td>".
              "<form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=sessionJoin value='Join Session'><input type='hidden' name='rowCRN' value=".$row['courseCRN']."></form>";
              echo "</tr>";

        }
           ?>


                </table>
            </div>

            <h1>
              <a href="create_session.php"> Start New Session</a>
            </h1>

          </div>

      </body>
</html>
