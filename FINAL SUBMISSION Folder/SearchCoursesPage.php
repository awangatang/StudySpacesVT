<?php
    session_start();
    $_SESSION["UserEmail"] = "sbrown@gmail.com";
    $email = $_SESSION["UserEmail"];
    require_once("db.php");
    $sql = "SELECT * FROM user WHERE userEmail = '$email'";
    $result = $mydb->query($sql);
    $row=mysqli_fetch_array($result);
    $userID = $row['userID'];

    if(isset($_POST['courseAdd'])){

        $newCRN = $_POST['rowCRN'];

        require_once("db.php");
        $sql = "SELECT * FROM usercourse WHERE userID = $userID AND courseCRN = $newCRN";
        $result = $mydb->query($sql);
        if(mysqli_num_rows($result) == 0){

            $sql = "INSERT INTO usercourse VALUES ($userID, $newCRN)";
            $result = $mydb->query($sql);

        } else{

            $message = "You Are Already In That Course!";
            echo "<script type='text/javascript'>alert('$message');</script>";

        }

    }

    if(isset($_POST['courseRemove'])){

        $newCRN = $_POST['rowCRN'];
        require_once("db.php");
        $sql = "DELETE FROM usercourse WHERE userID = $userID AND courseCRN = $newCRN";
        $result = $mydb->query($sql);

    }

?>

<!DOCTYPE html>
<html>

    <head>

        <title>Search Courses Page</title>
        <meta name="author" content="Jasmine Wang">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">  

    </head>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <style>
    
        #searchInfo, #courseInfo, #userCourses{
            background: lightgrey;
        }

        #searchInfo{
            position:relative;
            left:100px;
            top:150px;
            bottom: 500px;
            width: 25%;
            padding:25px 25px 15px 25px;
        }

        #userCourses{
            position:relative;
            left: 600px;
            bottom: 200px;
            width: 800px;
            height: 400px;
            padding: 10px 50px 20px 50px;
            margin: 0px 50px 10px 50px;
        }

        #bioHeader{
            position:relative;
            left: 650px;
            bottom: 420px;

        }

        #courseInfo{
            position:relative;
            bottom: 200px;
            left: 75px;
        }

        label {
            font-size:25px;
            position:relative;
            left: 85px;
        }

        #submit{
            position: relative;
            top: 100px;
            left: 125px;
        }

        button{
            position: relative;
            bottom: 500px;
            left: 222px;
        }

        td{
            text-align: center;
        }

        #error{
            color:red;
            font-style:italic;
        }

        .container-fluid{
            position:relative;
            left: 200px;
        }
    
    </style>
	<script src="jquery-3.1.1.min.js"></script>
    <script>
		$(function(){

			setInterval(updateTime, 1000);

		})

		function updateTime() {
			var d = new Date();
			var hours= d.getHours(),
					minutes=d.getMinutes(),
					seconds=d.getSeconds(),
					ampm = 'AM';

					if(hours>=12)
						ampm = 'PM';

					if(ampm == 'PM'){

						if(hours > 12){

							hours = hours - 12

						}

					}

			$("#current-time").text(hours + ":" + minutes +":"+seconds+" "+ampm);
		}

	    </script>

    <body>

        <div class="sidenav">
        <img class="logo" src="sslogo.png" width="125px">
        <ul class="nav nav-pills">
        <li><a href="#">Home</a></li>
        <li><a href="SessionPage.html">Session</a></li>
        <li><a href="#">Search</a></li>
        <li class="active"><a href="AccountManagement.php">Profile</a></li>
        <li><p id="current-time">00:00:00</p></li>
        </ul>
    </div>


        <div class="container-fluid">

            <div id="searchInfo">
                
                <form method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">

                    <label>Search & Filter Courses</label>
                    </br>
                    <select name = "searchType">
                        <option name=default value = "AllCourses">All Courses</option>
                        <option name=searchSubject value = "CourseSubject" <?php if (isset($_GET['submit']) && $_GET['searchType'] == 'CourseSubject') echo 'selected="selected"'; ?> >Course Subject</option>
                        <option name=searchCNum value = "CourseNumber" <?php if (isset($_GET['submit']) && $_GET['searchType'] == 'CourseNumber') echo 'selected="selected"'; ?> >Course Number</option>
                        <option name=searchCRN value = "CRN" <?php if (isset($_GET['submit']) && $_GET['searchType'] == 'CRN') echo 'selected="selected"'; ?> >CRN</option>
                        <option name=searchProf value = "Professor" <?php if (isset($_GET['submit']) && $_GET['searchType'] == 'Professor') echo 'selected="selected"'; ?> >Professor</option>
                    </select>
                    <input type="text" name="searchBox" id="searchBox">
                    <input type="submit" name="submit" value="Search">
                    <?php

                    ?>

                </form>
            
            </div>

            <h2 id="bioHeader">My Courses</h2>
            <div name="myCourses" id="userCourses">

                <table id="myClassInfo" border=1 width=700px>
                    <tr>
                        <th>Course #</th>
                        <th>CRN</th>
                        <th>Professor</th>
                        <th>Class Time</th>
                        <th></th>
                    </tr>
                    <?php

                        require_once("db.php");
                        $sql = "SELECT * FROM course INNER JOIN userCourse ON course.courseCRN = userCourse.courseCRN INNER JOIN professors ON course.professorID = professors.professorID WHERE userCourse.userID = '$userID'";
                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){

                            echo "<tr>";
                            echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['courseCRN']."</td><td>".$row['professorName']."</td><td>".$row['courseDays']." ".$row['courseTime']."</td><td>".
                            "<form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=courseRemove value='Remove Course'><input type='hidden' name='rowCRN' value=".$row['courseCRN']."></form>";
                            echo "</tr>";

                        }

                    ?>
                </table>


            </div>

                <table id="courseInfo" border=1 width="1500px">
                    <tr>
                        <th>Course #</th>
                        <th>CRN</th>
                        <th>Professor</th>
                        <th>Class Time</th>
                        <th></th>
                    </tr>
                    <?php

                        if(!isset($_GET['submit'])){

                            require_once("db.php");
                            $sql = "SELECT * FROM course INNER JOIN professors ON course.professorID = professors.professorID ";
                            $result = $mydb->query($sql);
                            while($row=mysqli_fetch_array($result)){
    
                                echo "<tr>";
                                echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['courseCRN']."</td><td>".$row['professorName']."</td><td>".$row['courseDays']." ".$row['courseTime']."</td><td>".
                                "<form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=courseAdd value='Add Course'><input type='hidden' name='rowCRN' value=".$row['courseCRN']."></form>";
                                echo "</tr>";
                            }

                        } else{

                            if($_GET['searchType'] != "AllCourses"){

                                $searchType = $_GET['searchType'];

                                if(isset($_GET['searchBox'])){

                                    $searchSet = true;

                                }

                                if($searchSet == true){

                                    $search = $_GET['searchBox'];

                                    switch ($searchType){

                                        case "CourseNumber":
                                            $filter = "courseNum";
                                            break;
                                        case "CourseSubject":
                                            $filter = "courseSubject";
                                            break;
                                        case "CRN":
                                            $filter = "courseCRN";
                                            break;
                                        case "Professor":
                                            $filter = "professorName";
                                            break;
                                        default:
                                            echo "Error";
                                            
                                    }

                                    require_once("db.php");
                                    $sql = "SELECT * FROM course INNER JOIN professors ON course.professorID = professors.professorID WHERE $filter LIKE '%$search%'";
                                    $result = $mydb->query($sql);
                                    while($row=mysqli_fetch_array($result)){
            
                                        echo "<tr>";
                                        echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['courseCRN']."</td><td>".$row['professorName']."</td><td>".$row['courseDays']." ".$row['courseTime']."</td><td>".
                                        "<form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=courseAdd value='Add Course'><input type='hidden' name='rowCRN' value=".$row['courseCRN']."></form>";
                                        echo "</tr>";
            
                                    }

                                }

                            } else{

                                require_once("db.php");
                                $sql = "SELECT * FROM course INNER JOIN professors ON course.professorID = professors.professorID ";
                                $result = $mydb->query($sql);
                                while($row=mysqli_fetch_array($result)){
        
                                    echo "<tr>";
                                    echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['courseCRN']."</td><td>".$row['professorName']."</td><td>".$row['courseDays']." ".$row['courseTime']."</td><td>".
                                    "<form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=courseAdd value='Add Course'><input type='hidden' name='rowCRN' value=".$row['courseCRN']."></form>";
                                    echo "</tr>";
        
                                }

                            }

                        }


                        
                    ?>
                </table>

        </div>
        
    </body>

</html>