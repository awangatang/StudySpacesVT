<?php
    session_start();
    $_SESSION["sessionID"] = 6;
    $sessID = $_SESSION["sessionID"];
    $_SESSION["userID"] = 6;
    $userID = $_SESSION["userID"];
    $_SESSION["noteID"] = 6;
    $noteID = $_SESSION["noteID"];

     if(isset($_POST['submit'])){
        // $name = $_FILES['file']['name'];
        // $temp_name  = $_FILES['file']['tmp_name'];
        $target_dir = "uploads/";
        $target_file = $target_dir.basename($_FILES["file"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $fileName=$_FILES["file"]["name"];
        $fileTmpName=$_FILES["file"]["tmp_name"];

        require_once("db.php");
        $uploadOk = 1;
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
      }
      else {
          if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
      //add to php sql
          $sql = "INSERT into notes(userID,sessionID, noteID, name)
              values('$userID','$sessID', '$noteID', '$fileName')";
          $result=$mydb->query($sql);
              echo "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.";
          } else {
              echo "Sorry, there was an error uploading your file.";
          }
      }
}

          if(isset($_POST['update'])){
             $target_dir = "uploads/";
             $target_file = $target_dir.basename($_FILES["file"]["name"]);
             $uploadOk = 1;
             $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
             $fileName=$_FILES["file"]["name"];
             $fileTmpName=$_FILES["file"]["tmp_name"];

             require_once("db.php");
             $uploadOk = 1;

           if ($uploadOk == 0) {
               echo "Sorry, your file was not uploaded.";
           }
           else {
               if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
               $sql = "INSERT into notes(userID,sessionID, noteID, name)
                   values('$userID','$sessID', '$noteID', '$fileName')";
               $result=$mydb->query($sql);
                   echo "The file ". basename( $_FILES["file"]["name"]). " has been updated.";
               } else {
                   echo "Sorry, there was an error uploading your file.";
               }
           }
          }

    if(isset($_POST["deleteNote"])){
      $noteID = $_POST["noteID"];
      require_once("db.php");
        $sql = "DELETE FROM notes WHERE userID = '$userID' AND noteID = '$noteID'";
        $result = $mydb->query($sql);
        $row = mysqli_num_rows($result);
        if($row != 1){
          $message = "Note Deleted.";
        }
        else{
          $message = "Invalid: This is another users note.";
        }
        echo "<script type='text/javascript'> alert('$message');</script>";
    }

?>

<!DOCTYPE html>
<html>

    <head>
        <title>Add Notes Page</title>
        <meta name="author" content="Neha Shah">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
    </head>

    <script src="js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <style>
    #addNotes{
        background: lightgrey;
    }
        center {
            display: relative;
            left: 50px;
            width: 75px;
          }
    </style>

    <body>

      <div class="sidenav">
        <img class="logo" src="sslogo.png" width="125px">
        <ul class="nav nav-pills">
          <li class="active"><a href="Homepage.php">Home</a></li>
          <li ><a href="SessionPage.php">Session</a></li>
          <li><a href="SearchInactiveSessionPage.php">Search</a></li>
          <li><a href="AccountManagement.php">Profile</a></li>
        </ul>
      </div>

        <div class="content">
          <h1>WOO! All Done Studying! Let's Update some notes! </h1>

          <img src= "book.jpg" width ="200" height= "200" class= "center"; alt="This a picture of an open book."/>
          <div id="addNotes"></div>

          <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" >
            <input type="file" name="file"/><br><br>
            <input type="submit" name="submit" id="submit" value="Upload Document">
          </form>

          <p name="notes" id="notes">
                         <table id="commentList" border=1 width="950px">
                             <tr>
                                 <th>Notes: </th>
                                 <th>File: </th>
                                 <th>Delete: </th>
                                 <th>Update: </th>
                             </tr>
                             <?php
                                  require_once("db.php");
                                  $sql = "SELECT * FROM notes INNER JOIN user ON notes.userID = user.userID
                                  WHERE sessionID = $sessID";
                                  $result = $mydb->query($sql);
                                  $i = 0;
                                  while($row=mysqli_fetch_array($result)){

                                   echo "<tr>";
                                   echo "<td>".$row['userFirstName']." ".$row['userLastName']."</td><td id = $i >".$row['name']."</td><td>
                                   <form method='post' action=".$_SERVER['PHP_SELF'].">
                                   <input class='delete' type='submit' name='deleteNote' value= 'Delete Note'>
                                   <input type='hidden' name='noteID' value=".$row['noteID']."></td><td>
                                   <input type='file' name='file'><input type='submit' name='update' id='update' value='Upload Document'></td>
                                   </form>";
                                 echo "</tr>";
                                 $i++;

                               }
                              ?>
        </div>
  </body>

  <body>
    <script src='https://d3js.org/d3.v4.min.js'></script>
    <h3>There are <script> d3.count([1, 2, "file"]) </script> notes for this!!</h3>
    <script>    d3.select('h3').style('color', 'darkblue'); d3.count([1, 2,]);   d3.select('h3').style('font-size', '24px');</script>
  </body>
</html>
