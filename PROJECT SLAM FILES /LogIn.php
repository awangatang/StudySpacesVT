<?php
if (isset($_POST['submit'])){
  if (isset($_POST['acctEmail'])){
    $userName = $_POST['acctEmail'];
  }
  if (isset($_POST['acctPass'])){
    $password = $_POST['acctPass'];
  }

  require_once("db.php");
  $sql = "SELECT * FROM user WHERE userPassWord = '$password' AND userEmail = '$userName'";
  $result = $mydb->query($sql);

  if (mysqli_num_rows($result)== 1){
    $row=mysqli_fetch_array($result);
    $userEmail = $row['userEmail'];
    $userID = $row['userID'];
    session_start();
    $_SESSION["UserEmail"] = $userEmail;
    $_SESSION["userID"] = $userID;
    Header("Location: Homepage.php");
    }

  else{
   echo "<p id = 'error'>Your Account Doesn't Exist</p>";
  }
}
?>

<!DOCTYPE html>
<html>

<head>
  <title>Log In Page</title>
  <meta name="author" content="Neha Shah">
  <link rel="stylesheet" type="text/css" href="studyspaceslogin.css">
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="js/bootstrap.min.js"></script>
</head>

<style>
  #userInfo,
  #classInfo,
  #acctBio {
    background: lightgrey;
  }

  #error{
    font-style:italic;
    color:red;
    position:relative;
    left: 450px;
    top: 300px;
  }

  #userInfo {
    position: relative;
    left: 100px;
    top: 10px;
    bottom: 500px;
    width: 40%;
    padding: 25px 25px 15px 25px;
  }

  input,  button {
    font-size: 20px
  }

  label {
    font-size: 25px;
  }

  #submit {
    position: relative;
    left: 125px;
  }

  #SignUp {
    position: relative;
    bottom: 450px;
    left: 100px;
  }

  button {
    position: relative;
    bottom: 500px;
    left: 222px;
  }

</style>


<body>

  <div class="sidenav">
      <img class="logo" src="sslogo.png" width="125px">
    </div>


  <div class="content">
    <h1>
      Welcome to
      <br>
      Study Spaces!
    </h1>

    <form id="userInfo" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <table>
        <tr>
          <td>
            <label>Email:</label>
          </td>
          <td>
            <input type="email" name="acctEmail">
          </td>
        </tr>
        <tr>
          <td>
            <label>Password:</label>
          </td>
          <td>
            <input type="password" name="acctPass">
          </td>
        </tr>
      </table>
      <input type="submit" name="submit" id="submit" value="Log In" />
    </form>

    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

    <h5 id="SignUp">New User?
    <a href="CreateAccount.php">Create Account Here</a>
    </h5>
  </div>
</body>

</html>
