<meta name="author" content="Emily Vang" />

<?php
session_start();


$course= $_POST["course"];
$location = $_POST["location"];
$desc = $_POST["desc"];
$sDate = getdate();
$hours = $_POST["hours"];
$min = $_POST["minutes"];
$sec = $_POST["seconds"];
$CRN = "";
$memberName = "";
$st = date("H:i:s");
$date = date("Y-m-d");
$split = explode(" ", $course);

$subject = $split[0];
$number = $split[1];

require_once("db.php");

$sql = "SELECT courseCRN FROM course
WHERE course.courseSubject  = '$subject' AND course.courseNum = '$number'";
$result = $mydb->query($sql);
$row=mysqli_fetch_array($result);
$CRN = $row[0];

$sql = "insert into sessions(courseCRN, sessionStatus, sessionDate, sessionST, sessionLocation)
values('$CRN', 'active', '$date', '$st', '$location')";
$result=$mydb->query($sql);

$sql = "SELECT LAST_INSERT_ID();";
$result=$mydb->query($sql);
$row=mysqli_fetch_array($result);
$sID = $row[0];

$_SESSION["sessionID"] = $sID;

$sql = "SELECT userFirstName, userLastName FROM user
INNER JOIN userSession on userSession.userID = user.userID
WHERE userSession.sessionID = '$sID'";
$result=$mydb->query($sql);
$row=mysqli_fetch_array($result);
$memberName = $row[0]." ".$row[1];

?>

<!DOCTYPE html>
<html>

<head>
  <title>Session Page</title>
  <link rel="stylesheet" href="studyspaces.css">
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="js/bootstrap.min.js"></script>
  <style media="screen">
    #timerContainer {
      font-family: roboto;
      font-weight: 300;
      height: 346px;
      width: 346px;
      border-radius: 50%;
      display: inline-block;

      position: absolute;
      width: 346px;
      height: 346px;
      left: 498px;
      top: 177px;

      background: #C4C4C4;

    }

    #timer {

      width: 54%;
      padding: 20px 0;
      font-family: Roboto;
      font-style: normal;
      font-weight: 900;
      font-size: 72px;
      line-height: 84px;
      display: flex;
      align-items: center;
      text-align: center;

      color: #000000;
      cursor: pointer;
      position: relative;
      top: 35%;
      transform: translateX(16%);
    }

    .endSession {
      color: white;
      text-align: center;
      position: absolute;
      width: 327px;
      height: 55px;
      left: 517px;
      top: 565px;

      background: #C4C4C4;
      box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
      border-radius: 10px;
    }

    .overview {
      position: absolute;
      width: 327px;
      height: 55px;
      left: 517px;
      top: 78px;

      background: #C4C4C4;
      box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
      border-radius: 10px;

    }

    .overview:hover {
      background: #22c6bd;
    }

    .endSession-text {
      position: absolute;
      width: 552px;
      height: 57px;
      left: 100px;

      font-family: Roboto;
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 28px;
      display: flex;
      align-items: center;
      text-align: center;

      color: #000000;
    }

    .overview-text {
      position: absolute;
      width: 552px;
      height: 57px;
      left: 65px;

      font-family: Roboto;
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 28px;
      display: flex;
      align-items: center;
      text-align: center;

      color: #000000;
    }


    .endSession:hover {
      background: #22c6bd;
    }

    .lighter {
      background: #CC0000
    }

    #overlay, #overview {
      position: fixed;
      /* Sit on top of the page content */
      display: none;
      /* Hidden by default */
      width: 100%;
      /* Full width (cover the whole page) */
      height: 100%;
      /* Full height (cover the whole page) */
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: rgba(0, 0, 0, 0.5);
      /* Black background with opacity */
      z-index: 2;
      /* Specify a stack order in case you're using a different order for other elements */
      cursor: pointer;
      /* Add a pointer on hover */
    }

    #endAlert, #overview_popup {
      position: absolute;
      width: 654px;
      height: 368px;
      left: 369px;
      top: 168px;

      background: #C4C4C4;
      border-radius: 20px;
    }

    .alert, popup {
      position: absolute;
      width: 477px;
      height: 94px;
      top: 100px;
      left: 100px;

      font-family: Roboto;
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 28px;
      display: flex;
      align-items: center;
      text-align: center;

      color: #000000;
    }

    .yes_btn {
      position: absolute;
      width: 188px;
      height: 49px;
      left: 457px;
      top: 408px;

      background: #FFFFFF;
      border-radius: 10px;
    }

    .no_btn {

      position: absolute;
      width: 188px;
      height: 49px;
      left: 746px;
      top: 408px;

      background: #FFFFFF;
      border-radius: 10px;
    }

    #exit {
      position: absolute;
      width: 32px;
      height: 32px;
      left: 610px;
      top: 10px;
      border-radius: 50%;

      background: #E5E5E5;
    }

    h1 {
      position: absolute;
      width: 552px;
      height: 57px;
      left: 550px;
      top: 8px;

      font-family: Roboto;
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 28px;
      display: flex;
      align-items: center;
      text-align: center;

      color: #000000;
    }
  </style>
</head>

<body>
  <div id="overview">
    <div id="overview_popup">
      <div class="popup">
        <h3>Session Overview</h3>
          <table border="1">
            <tr>
              <td><strong>Course Name:</strong></td>
              <td><?php echo $course; ?></td>
              <td><strong>Location:</strong></td>
              <td><?php echo $location; ?></td>
            </tr>
            <tr>
              <td><strong>Date:</strong></td>
              <td><?php echo "$sDate[weekday], $sDate[month] $sDate[mday], $sDate[year]" . "<br>";?></td>
              <td><strong>Description:</strong></td>
              <td><?php echo $desc; ?></td>
            </tr>
            <tr>
              <td><strong>Time:</strong></td>
              <td><?php echo $st;?></td>
            </tr>
          </table>

        <h3>Members in Session</h3>
        <table border="1">
          <tr>
            <td><?php echo $memberName; ?></td>
          </tr>
        </table>
      </div>
        <button type="button" onclick="off()" id="exit" name="exit_btn">X</button>
    </div>
  </div>
  <div id="overlay">
    <div id="endAlert">
      <div class="alert">Are you sure you want to end this session?</div>
    </div>
    <button onclick="document.location.href = 'end_session.php'" type="button" class="yes_btn">Yes</button>
    <button onclick="off()" type="button" class="no_btn">No</button>
  </div>
  <div class="sidenav">
    <img class="logo" src="sslogo.png" width="125px">
    <ul class="nav nav-pills">
      <li><a href="#">Home</a></li>
      <li class="active"><a href="SessionPage.html">Session</a></li>
      <li><a href="#">Search</a></li>
      <li><a href="#">Profile</a></li>
    </ul>
  </div>
  <div class="content">
    <h1><?php echo $course." Study Session"; ?></h1>
    <div class="overview" onclick="showOveriew()">
      <div class="overview-text">Session Overview</div>
    </div>
    <div id="timerContainer">
      <div id="timer" data-toggle="tooltip" data-placement="right" title="Click to start session!">00:00:00</div>
    </div>
    <div class="endSession" onclick="endTimer()">
      <div class="endSession-text">End Session</div>
    </div>
  </div>
  <script type="text/javascript">


var hours = <?php echo $hours ?>;
var minutes = <?php echo $min ?>;
var seconds = <?php echo $sec ?>;
var interval = null;

console.log(hours);

document.getElementById('timer').addEventListener('click', () => {
var timeInSeconds = (hours * 60 * 60) +
  (minutes * 60) +
  seconds;

var displayTime = () => {
  var displayHours = Math.floor(timeInSeconds / (60 * 60));
  console.log(displayHours);
  var remainder = timeInSeconds - (displayHours * 60 * 60);
  var displayMinutes = Math.floor(remainder / 60);
  var displaySeconds = remainder - (displayMinutes * 60);

  if (displayMinutes < 10) {
    if (displaySeconds < 10) {
      document.getElementById("timer").innerHTML = displayHours + ":0" +
      displayMinutes + ":0" + displaySeconds;
    } else {
      document.getElementById("timer").innerHTML = displayHours + ":0" +
      displayMinutes + ":" + displaySeconds;
    }
  }
  else {
    if (displaySeconds < 10) {
      document.getElementById("timer").innerHTML = displayHours + ":" +
      displayMinutes + ":0" + displaySeconds;
    } else {
      document.getElementById("timer").innerHTML = displayHours + ":" +
      displayMinutes + ":" + displaySeconds;
    }
  };

  /*document.getElementById("timer").innerHTML = displayHours + ":" +
  displayMinutes + ":" + displaySeconds;*/
};
interval = setInterval(() => {
  displayTime();
  timeInSeconds -= 1;
  if (timeInSeconds < 0) {
    clearInterval(interval);
  }
}, 1000);
});
function endTimer() {

    document.getElementById("overlay").style.display = "block"; //displays overlay and end session alert

}

function showOveriew(){
    document.getElementById("overview").style.display = "block";
}

function off() {
  document.getElementById("overlay").style.display = "none";
  document.getElementById("overview").style.display = "none";
}
  </script>
</body>

</html>
