<meta name="author" content="Jordan Miers" />
<?php
    session_start();
    $search = $_SESSION["sbox"];
    $_SESSION["UserEmail"] = "sbrown@gmail.com";
    $email = $_SESSION["UserEmail"];
    $filter = $_SESSION["searchType"];
    echo $filter;
    require_once("db.php");
    $sql = "SELECT * FROM user WHERE userEmail = '$email'";
    $result = $mydb->query($sql);
    $row=mysqli_fetch_array($result);
    $userID = $row['userID'];
?>

<!doctype html>
<html>

    <head>

        <title>Session History</title>
        <meta name="author" content="You">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">
        <script src="js/bootstrap.min.js"></script>
        <link href="css/bootstrap.min.css" rel="stylesheet" />
    </head>

    <style>

        h1{
            text-align:center;
            vertical-align: top;
        }

        #sessionInfo{
            position: relative;
            left: 125px;
            top: 10px;
        }

    </style>

    <div class="sidenav">
      <img class="logo" src="sslogo.png" width="125px">
        <ul class="nav nav-pills">
          <li><a href="">Home</a></li>
          <li><a href="SessionPage.html">Session</a></li>
          <li class="active"><a href="SearchInactiveSessionPage.php">Search</a></li>
          <li><a href="#">Profile</a></li>
        </ul>
    </div>
<script>
    function sortTable(n) {
      var table, rows, switching, i, x, y, shouldSwitch;
      table = document.getElementById("Info");
      switching = true;

      while (switching) {
        switching = false;
        rows = table.rows;

        for (i = 1; i < (rows.length - 1); i++) {
          shouldSwitch = false;

          x = rows[i].getElementsByTagName("TD")[0];
          y = rows[i + 1].getElementsByTagName("TD")[0];
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        }
        if (shouldSwitch) {

          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
        }
      }
    }
    </script>
    <body>

        <div class="content">

            <h1>Inactive Session Search Results</h1>
            </br>

            <table id="Info" border=1 width="1500px">
                    <tr>
                        <th onclick="sortTable[0]">Course #</th>
                        <th onclick="sortTable[1]">Session Status</th>
                        <th onclick="sortTable[2]">Professor</th>
                        <th onclick="sortTable[3]">Session Time</th>
                        <th onclick="sortTable[4]">Session Info</th>
                    </tr>

                    <?php

                        require_once("db.php");

                        if($filter != "courseCRN"){

                          if ($filter != "Filters"){
                            $sql = "SELECT * FROM sessions
                                INNER JOIN usersession ON sessions.sessionID = usersession.sessionID
                                INNER JOIN course ON sessions.courseCRN = course.courseCRN
                                INNER JOIN professors ON course.professorID = professors.professorID
                                WHERE sessionStatus = 'Inactive' AND $filter LIKE '%$search%'";
                              } else {

                                $sql = "SELECT * FROM sessions
                                    INNER JOIN usersession ON sessions.sessionID = usersession.sessionID
                                    INNER JOIN course ON sessions.courseCRN = course.courseCRN
                                    INNER JOIN professors ON course.professorID = professors.professorID
                                    WHERE sessionStatus = 'Inactive'";

                              }
                          } else {

                            $sql = "SELECT * FROM sessions
                                INNER JOIN usersession ON sessions.sessionID = usersession.sessionID
                                INNER JOIN course ON sessions.courseCRN = course.courseCRN
                                INNER JOIN professors ON course.professorID = professors.professorID
                                WHERE sessionStatus = 'Inactive' AND sessions.$filter LIKE '%$search%'";

                          }

                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){

                            echo "<tr>";
                            echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['sessionStatus']."</td><td>".$row['professorName']."</td><td>".$row['sessionDate']." ".$row['sessionST']." - ".$row['sessionET']."</td><td>".$row['sessionLocation'];
                            echo "</tr>";

                        }

                    ?>
                  <button> <a href="InactiveSessionResultDetails.php">See the Result Details! </a></button>

            </table>

        </div>
    </body>
</html>
