<?php
  $course = "";
  $location = "";
  $desc = "";
  $sDate = getdate();
  $hours = "";
  $min = "";
  $sec = "";
  $CRN = "123";
  $active = "active";
  $rating = 5;
  $t = new DateTime();

  if (isset($_POST["submit"])) {
      if(isset($_POST["course"])) $course=$_POST["course"];
      if(isset($_POST["location"])) $location=$_POST["location"];
      if(isset($_POST["desc"])) $desc=$_POST["desc"];
      if(isset($_POST["hours"])) $hours=$_POST["hours"];
      if(isset($_POST["minutes"])) $min=$_POST["minutes"];
      if(isset($_POST["seconds"])) $sec=$_POST["seconds"];

      header("Location: session_new_test.php");
    }

 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="author" content="Emily Vang" />

  <title>Create Session</title>
  <link rel="stylesheet" href="studyspaces.css">
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="js/bootstrap.min.js"></script>

  <script src="jquery-3.1.1.min.js"></script>
  <script type="text/javascript"></script>
  <style media="screen">
    .content label {
      position: fixed;
    }

    .content input,
      select, textarea {
      margin-left: 250px;
    }
    .study {
      font-size: 18px;
      position: absolute;
      width: 300px;
      height: 40px;
      left: 325px;
      top: 450px;

      background: #C4C4C4;
      box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
      border-radius: 10px;
    }

    #hours {
      position: absolute;
      width: 34px;
      height: 32px;
      left: 210px;
      top: 280px;
    }
    #minutes {
      position: absolute;
      width: 34px;
      height: 32px;
      left: 270px;
      top: 280px;
    }

    #seconds {
      position: absolute;
      width: 34px;
      height: 32px;
      left: 330px;
      top: 280px;
    }
    .study:hover {background-color: #22c6bd}
  </style>
</head>

<body>
  <div class="sidenav">
    <img class="logo" src="sslogo.png" width="125px">
    <ul class="nav nav-pills">
      <li><a href="#">Home</a></li>
      <li class="active"><a href="session_start.html">Session</a></li>
      <li><a href="#">Search</a></li>
      <li><a href="#">Profile</a></li>
    </ul>
  </div>
  <div class="content">

    <form class="search" form method="post" action="session_clock.php">
      <label for="course">What course are you studying for?</label>
      <select class="" name="course">
        <?php
        require_once("db.php");
        $sql = "SELECT courseSubject, courseNum,
          CONCAT(courseSubject, ' ', courseNum) courseName
          FROM course
          ORDER BY courseName";
        $result = $mydb->query($sql);
        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["courseName"]."'>".$row["courseName"]."</option>";
        }
         ?>
      </select>
      <br>
      <br>
      <label for="location">Study location?</label>
      <input type="text" name="location" value="">
      <br>
      <br>
      <label for="">Study Time</label>
      <input type="text" name="hours" id="hours" value="Hr/s">
      <input type="text" name="minutes" id="minutes" value="Min">
      <input type="text" name="seconds" id="seconds" value="Sec">
      <br>
      <br>
      <label for="desc">Add a description</label>
      <br>
      <textarea name="desc" rows="3" cols="36">Write a description for your session here!</textarea>
      <br>
      <button type="submit" class="study">Start Studying!</button>
    </form>
  </div>
</body>

</html>
