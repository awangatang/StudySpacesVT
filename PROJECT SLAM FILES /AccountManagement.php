<?php

    session_start();
    if(isset($_POST['submit'])){

        $userID = $_POST['acctID'];
        $userEmail = $_POST['acctEmail'];
        $userMajor = $_POST['acctMajor'];
        $userName = $_POST['acctName'];
        $splitName = explode(" ", $userName);
        $fName = $splitName[0];
        $lName = $splitName[1];

        if (empty($_POST['acctPW'])){

            require_once("db.php");
            $sql = "UPDATE user SET userFirstName = '$fName', userLastName = '$lName', userEmail = '$userEmail', userMajor = '$userMajor' WHERE userID = $userID";
            $result = $mydb->query($sql);
            $message = "User Info Updated";

        } else {
            
            $userPW = $_POST['acctPW'];
            require_once("db.php");
            $sql = "UPDATE user SET userFirstName = '$fName', userLastName = '$lName', userEmail = '$userEmail', userMajor = '$userMajor', userPassWord = '$userPW'  WHERE userID = $userID";
            $result = $mydb->query($sql);
            $message = "User Info And Password Updated";

        }

        if($result == 1){

            $_SESSION["UserEmail"] = $userEmail;
            echo "<script type='text/javascript'>alert('$message');</script>";

        }

    } else{

        $_SESSION["UserEmail"] = "sbrown@gmail.com";
        $email = $_SESSION["UserEmail"];

        require_once("db.php");
        $sql = "SELECT * FROM user WHERE userEmail = '$email'";
        $result = $mydb->query($sql);
        $row=mysqli_fetch_array($result);
        $userEmail = $row['userEmail'];
        $userName = $row['userFirstName']." ".$row['userLastName'];
        $userMajor = $row['userMajor'];
        $userID = $row['userID'];

    }

    if(isset($_POST['courseInfo'])){

        $_SESSION['CRNOverview'] = $_POST['crnSession'];
        header("Location:courseOverview.php");

    }

?>
<!DOCTYPE html>
<html>

    <head>

        <title>Account Management Page</title>
        <meta name="author" content="Jasmine Wang">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">

    </head>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
    <style>
    
        #userInfo, #classInfo, #acctBio{
            background: lightgrey;
        }

        #userInfo{
            position:relative;
            left:100px;
            top:10px;
            bottom: 500px;
            width: 25%;
            padding:25px 25px 15px 25px;
        }

        #acctBio{
            position:relative;
            left: 600px;
            bottom: 420px;
            width: 800px;
            height: 500px;
            padding: 10px 50px 20px 50px;
            margin: 0px 50px 10px 50px;
        }

        #bioHeader{
            position:relative;
            left: 650px;
            bottom: 420px;

        }

        #classInfo{
            position:relative;
            bottom: 300px;
            left: 75px;
        }

        input, button{
            font-size: 20px
        }

        label {
            font-size:25px;
        }

        #submit{
            position: relative;
            left: 125px;
        }

        button{
            position: relative;
            bottom: 500px;
            left: 222px;
        }
    
        h2{
            position: relative;
            bottom: 300px;
            left: 650px;
        }

        th{
            text-align:center;
        }

    </style>

    <script>

        function init(){

            var c = document.getElementById("submit");
            c.addEventListener("click", checkInput, false)

        }

        function checkInput(){

            var z = document.getElementById("acctN")
            var errormsg = document.getElementById("errorMsg")
            var error = false

            if (z.value == ""){

                error = true

            }

            if (error = true){

                errormsg.innerhtml = "All fields must be filled out to edit user info (except Password)"

            } else {

                errormsg.innerhtml = "Everythings gucci"

            }

        }

        document.addEventListener("DOMContentLoaded", init);
    
    </script>

    <body>

        <div class="sidenav">
        <img class="logo" src="sslogo.png" width="125px">
        <ul class="nav nav-pills">
            <li><a href="#">Home</a></li>
            <li><a href="SessionPage.html">Session</a></li>
            <li><a href="#">Search</a></li>
            <li class="active"><a href="#">Profile</a></li>
        </ul>
   
        </div>

        

        <div class="content">

            <form id="userInfo" name="userForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >

                <p id = "errorMsg"></p>

                <table>
                    <tr>
                        <td>
                        <label>Name:</label>
                        </td>
                        <td>
                        <input type="text" name="acctName" id="acctN" value="<?php echo $userName ?>" required>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <label>Email:</label>
                        </td>
                        <td>
                        <input type="email" name="acctEmail" value="<?php echo $userEmail ?>" required>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <label>Major:</label>
                        </td>
                        <td>
                        <input type="text" name="acctMajor" value="<?php echo $userMajor ?>" required>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <label>Password:</label>
                        </td>
                        <td>
                        <input type="password" name="acctPW">
                        </td>
                    </tr>
                </table>

                <input type="hidden" name="acctID" value="<?php echo $userID ?>">

                <input type="submit" name="submit" id="submit" value="Change Info" />
            
            </form>

            <h2 id="bioHeader">About Me</h2>
            <p name="AboutMe" id="acctBio">

                <textarea rows = 10>



                </textarea>

            </p>

            <button onclick="window.location.href = 'SessionHistory.php';">View Session History</button>

            <button onclick="window.location.href = 'SearchCoursesPage.php';">Add Classes</button>

                <h2>Your Courses</h2>
                <table id="classInfo" border=1 width="1500px">
                    <tr>
                        <th>Course #</th>
                        <th>CRN</th>
                        <th>Professor</th>
                        <th>Class Time</th>
                        <th>Course Info</th>
                    </tr>
                    <?php

                        require_once("db.php");
                        $sql = "SELECT * FROM course INNER JOIN userCourse ON course.courseCRN = userCourse.courseCRN INNER JOIN professors ON course.professorID = professors.professorID WHERE userCourse.userID = '$userID'";
                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){

                            echo "<tr>";
                            echo "<td>".$row['courseSubject']." ".$row['courseNum']."</td><td>".$row['courseCRN']."</td><td>".$row['professorName']."</td><td>".$row['courseDays']." ".$row['courseTime']."</td><td><form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=courseInfo value='View Course Info'><input type=hidden name=crnSession value =".$row['courseCRN']."></form>";
                            echo "</tr>";

                        }

                    ?>
                </table>

        </div>
        
    </body>

</html>