<meta name="author" content="Jordan Miers" />
<!doctype html>
<html>
<head>
  <title>Inactive Search Result Details</title>
  <meta name="author" content="Jordan Miers">
  <link rel="stylesheet" type="text/css" href="studyspaces.css">
  <script src="js/bootstrap.min.js"></script>
  <link href="css/bootstrap.min.css" rel="stylesheet" />
</head>

 <style>
    #SessionValues, #Notes, #Comments {
      background: #C4C4C4;
      border-spacing: 10px;
    }
    h1{
      text-align: center;

    }

    .notes {
      margin-left: 800px;
      margin-top: -45px;
    }

    #Notes {
      margin-left: 800px;
      margin-top: -265px;
    }

    #Comments {
      margin-top: 220px;
    }

    .comments {
      margin-top: 100px;
    }

.graphs{
  position:relative;
  align:center;
}
 </style>

  <div class="sidenav">
      <img class="logo" src="sslogo.png" width="125px" height="125px">
      <a href="Homepage.html">Home</a>
      <a href="SessionPage.html">Session</a>
      <li class="active"><a href="SearchInactiveSessionPage.php">Search</a></li>
      <a href="AccountManagement.php">Profile</a>
  </div>
  <h1>    Inactive Search Result Details Page</h1>
  <div class="content">
    <?php
      require_once("db.php");
      session_start();
        echo "<p>Session # ".$_SESSION["sessionID"];
        echo "<table id='SessionValues'";
        echo "<tr>";
        echo "<td>Course</td><td>BIT 4454";
        echo "</tr>";
        echo "<tr>";
        echo "<td>CRN</td><td>32254</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>Instructor</td><td>Shen</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>Time</td><td>MWF 10:30 - 11:45";
        echo "</tr>";
    ?>

  </div>
</div>
<div class ="graphs">
    <svg width="400" height="500"><g transform="translate(40,20)"><g class="axis axis--x" fill="none" font-size="10" font-family="sans-serif" text-anchor="middle" transform="translate(0,450)"><path class="domain" stroke="#000" d="M0.5,6V0.5H900.5V6"></path><g class="tick"
      opacity="1" transform="translate(225,0)"><line stroke="#000" y2="6"></line><text fill="#000" y="9" dy="0.71em">Average # Of Comments Made </text></g></g><g class="axis axis--y" fill="none" font-size="10" font-family="sans-serif" text-anchor="end">
        <path class="domain" stroke="#000" d="M-6,450.5H0.5V0.5H-6"></path><g class="tick" opacity="1" transform="translate(0,450.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">0</text></g>
        <g class="tick" opacity="1" transform="translate(0,395.5)"><line stroke="#000" x2="-6"></line><text x="-9" dy="0.32em" fill="#000">0.5</text></g><g class="tick" opacity="1" transform="translate(0,340.5)">
          <line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">1</text></g><g class="tick" opacity="1" transform="translate(0,286.5)"><line stroke="#000" x2="-6"></line>
            <text fill="#000" x="-9" dy="0.32em">1.5</text></g><g class="tick" opacity="1" transform="translate(0,231.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">2</text></g>
            <g class="tick" opacity="1" transform="translate(0,176.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">2.5</text></g><g class="tick" opacity="1" transform="translate(0,121.5)">
            <line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">3</text></g><g class="tick" transform="translate(0,67.5)" opacity="1"><line stroke="#000" x2="-6"></line>
              <text fill="#000" x="-9" dy="0.32em">3.5</text></g><g class="tick" opacity="1" transform="translate(0,12.5)"><line stroke="#000" x2="-6"></line><text fill="#000" x="-9" dy="0.32em">4</text></g>
              <text transform="rotate(-90)" y="6" dy="0.71em" text-anchor="end">Frequency</text></g><rect class="bar" x="82" y="0" width="736" height="450"></rect></g></svg>

<script src="https://d3js.org/d3.v4.min.js"></script>
<script>

var svg = d3.select("svg"),
    margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom;

var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
    y = d3.scaleLinear().rangeRound([height, 0]);
    console.log(y(3));

var g = svg.append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//specify our data source
/*
d3.tsv("data.tsv", function(d) {
  d.frequency = +d.frequency;
  return d;
}, function(error, data) {
*/

d3.json("getData.php", function(error, data){
  if(error) throw error;

  data.forEach(function(d){
    d.letter = d.ProductName;
    d.frequency = +d.Total_InStock_Value;
  })

  console.log(data);

  if (error) throw error;

  x.domain(data.map(function(d) { return d.letter; }));
  y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

  g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));

  g.append("g")
      .attr("class", "axis axis--y")
      .call(d3.axisLeft(y).ticks(10, "s"))
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Frequency");

  g.selectAll(".bar")
    .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.letter); })
      .attr("y", function(d) { return y(d.frequency); })
      .attr("width", x.bandwidth())
      .attr("height", function(d) { return height - y(d.frequency); });
});

</script>
</div>
  <div class="notes">
    <p>Notes</p>
    <?php
        echo "<table id='Notes'";
        echo "<tr>";
        echo "<td>You don't have any notes, add more notes!</td>"; //put in sql with notes
        echo "</tr>";
    ?>
  </div>
  <body>

  <div class = "comments">

      <p>Comments</p>
      <button> <a href="TrialComments.php">Click to view, add, delete, and edit the comments!</a></button>

      <?php
      require_once("db.php");
      $filter = $_SESSION["sessionID"];
      $sql = "select c.comment
      from comments c
      where c.sessionID = $filter";

      echo "<table id='Comments'>";

      $result = $mydb->query($sql);
      while($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>".$row["comment"]."</td>"; //put in sql with notes
        echo "</tr>";


      }
      echo "</table>"
      ?>


</body>
</html>
