<!doctype html>
<html>
<head>
  <title>Search Results</title>
  <meta name="author" content="Edward Reed">
  <link rel="stylesheet" type="text/css" href="studyspaces.css">
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="js/bootstrap.min.js"></script>
</head>

<style>

table {
  background: lightgrey;
}

td {
    text-align: center;
}
</style>

<body>
  <div class="sidenav">
    <img class="logo" src="sslogo.png" width="125px">
    <ul class="nav nav-pills">
      <li><a href="Homepage.php">Home</a></li>
      <li class="active"><a href="SessionPage.php">Session</a></li>
      <li><a href="SearchInactiveSessionPage.php">Search</a></li>
      <li><a href="AccountManagement.php">Profile</a></li>
    </ul>
  </div>
  <div class="content">



 <?php

 if (isset($_POST["reset"])) {

   Header("Location: searchOpenSession.php");
}

if (isset($_POST["joinSession"])) {

    $sessionID = $_POST["rowCRN"];

    session_start();
    $_SESSION["sessionID"] = $sessionID;

    require_once("db.php");
    //update to have userID from login
    $sql = "INSERT INTO usersession VALUES ('6', $sessionID)";
    $result = $mydb->query($sql);



    Header("Location: openSessionClock.php");
}
   session_start();
   require_once("db.php");

   $filter = $_SESSION['select'];
   $where = $_SESSION['entry'];


   switch($filter) {
     case "CRN":
       $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
               from sessions s, course c
               where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseCRN = $where";
        break;
    case "Course Number":
      $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
              from sessions s, course c
              where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseNum = $where";
        break;
    case "Course Subject":
        switch($where) {
          case "MKTG":
          $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                  from sessions s, course c
                  where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'MKTG'";
          break;
        case "BIT":
          $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                  from sessions s, course c
                  where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'BIT'";
          break;
        case "HNFE":
        $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                from sessions s, course c
                where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'HNFE'";
        break;
        case "CS":
        $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                from sessions s, course c
                where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'CS'";
        break;
        case "CMDA":
        $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                from sessions s, course c
                where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'CMDA'";
        break;
        case "FMD":
        $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                from sessions s, course c
                where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'FMD'";
        break;
        case "HD":
        $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                from sessions s, course c
                where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'HD'";
        break;
        case "APSC":
        $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                from sessions s, course c
                where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'ASPC'";
        break;
        case "BIO":
        $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                from sessions s, course c
                where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'BIO'";
      break;
      case "CHEM":
      $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
              from sessions s, course c
              where s.sessionStatus = 'active' AND s.courseCRN = c.courseCRN AND c.courseSubject = 'CHEM'";
        }

        break;
    default:
        $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation, s.sessionID
                from sessions s, course c
                where s.sessionStatus = 'active' AND s.courseCRN = s.courseCRN";
        break;
   }

  $result = $mydb->query($sql);
   echo "<table id='Results' border=1 width=800px >";
   echo "<thead>";
   echo "<th>Course CRN</th><th>Subject</th><th>Course #</th><th>Date</th><th>Start Time</th><th>Location</th><th>Join?</th>";
   echo "</thead>";

   while($row = mysqli_fetch_array($result)) {
     echo "<tr>";
     echo "<td>".$row["courseCRN"]."</td><td>".$row["courseSubject"]."</td><td>".$row["courseNum"]."</td><td>".$row["sessionDate"]."</td><td>".$row["sessionST"]."</td><td>".$row["sessionLocation"]."<td> <form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=joinSession value='Join'><input type='hidden' name='rowCRN' value=".$row['sessionID']."></form> </td>";
     echo "</tr>";
   }

     echo "<form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=reset value='Reset Search'><input type='hidden' name='rowCRN' value=".$row['courseCRN']."></form>";
  ?>
  </div>
</body>
</html>
