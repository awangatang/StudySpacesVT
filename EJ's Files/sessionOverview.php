<!doctype html>
<html>
<head>
  <title>Search Results</title>
  <meta name="author" content="Edward Reed">
  <link rel="stylesheet" type="text/css" href="studyspaces.css">
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="js/bootstrap.min.js"></script>
</head>

 <style>
    #SessionValues, #Notes, #Comments {
      background: #C4C4C4;
      border-spacing: 10px;
    }
    .notes {
      margin-left: 800px;
      margin-top: -45px;
    }

    #Notes {
      margin-left: 800px;
      margin-top: -265px;
    }

    #Comments {
      margin-top: 220px;
    }

    .comments {
      margin-top: 100px;
    }

 </style>
<body>
  <div class="sidenav">
    <img class="logo" src="sslogo.png" width="125px">
    <ul class="nav nav-pills">
      <li><a href="Homepage.php">Home</a></li>
      <li class="active"><a href="SessionPage.php">Session</a></li>
      <li><a href="SearchInactiveSessionPage.php">Search</a></li>
      <li><a href="AccountManagement.php">Profile</a></li>
    </ul>
  </div>
  <div class="content">
    <?php
      require_once("db.php");
      session_start();
        echo "<p>Session # ".$_SESSION["sessionID"];
        echo "<table id='SessionValues'";
        echo "<tr>";
        echo "<td>Course</td><td>MKTG 8994";
        echo "</tr>";
        echo "<tr>";
        echo "<td>CRN</td><td>57331</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>Instructor</td><td>Fowler</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>Time</td><td>MWF 11:00 - 12:15";
        echo "</tr>";
    ?>

  </div>

  <div class="notes">
    <p>Notes</p>
    <?php
        echo "<table id='Notes'";
        echo "<tr>";
        echo "<td>This session currently has no notes!</td>"; //put in sql with notes
        echo "</tr>";
    ?>
  </div>

  <div class = "comments">
      <p>Comments</p>
      <?php
      require_once("db.php");
      $filter = $_SESSION["sessionID"];
      $sql = "select c.comment
      from comments c
      where c.sessionID = $filter";

      echo "<table id='Comments'";

      $result = $mydb->query($sql);
      while($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        echo "<td>".$row["comment"]."</td>"; //put in sql with notes
        echo "</tr>";
      }
      ?>
  </div>
</body>
</html>
