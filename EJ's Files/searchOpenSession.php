<?php
    session_start();
    $email = $_SESSION["UserEmail"];


    require_once("db.php");
    $sql = "SELECT * FROM user WHERE userEmail = '$email'";
    $result = $mydb->query($sql);
    $row=mysqli_fetch_array($result);
    $userID = $row['userID'];

    if(isset($_POST['courseAdd'])){

        $newCRN = $_POST['rowCRN'];

        require_once("db.php");
        $sql = "SELECT * FROM usercourse WHERE userID = $userID AND courseCRN = $newCRN";
        $result = $mydb->query($sql);
        if(mysqli_num_rows($result) == 0){

            $sql = "INSERT INTO usercourse VALUES ($userID, $newCRN)";
            $result = $mydb->query($sql);

        } else{

            $message = "You Are Already In That Course!";
            echo "<script type='text/javascript'>alert('$message');</script>";

        }

    }

    if(isset($_POST['sessionRemove'])){

        //$value = $_POST["rowCRN"];
        require_once("db.php");
        //$sql = "DELETE FROM usercourse WHERE userID = $userID AND courseCRN = $newCRN";
        $sql = "DELETE FROM usercourse WHERE userID = '2' AND courseCRN = '2'";
        $result = $mydb->query($sql);

    }

    $select = "";
    $entry = "";
    if (isset($_POST["submit"])) {
      if(isset($_POST["searchType"])) $select=$_POST["searchType"];
      if(isset($_POST["searchBox"])) $entry=$_POST["searchBox"];

      session_start();
      $_SESSION["select"] = $select;
      $_SESSION["entry"] = $entry;
      Header("Location: OpensessionResults.php");
   }
?>

<!DOCTYPE html>
<html>

    <head>

        <title>Search Open Sessions Page</title>
        <meta name="author" content="Edward Reed">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <script src="js/bootstrap.min.js"></script>
    </head>

    <style>

        #searchInfo, #courseInfo, #userCourses{
            background: lightgrey;
        }

        #searchInfo{
            position:relative;
            left:100px;
            top:150px;
            bottom: 500px;
            width: 25%;
            padding:25px 25px 15px 25px;
        }

        #userCourses{
            position:relative;
            left: 600px;
            bottom: 200px;
            width: 800px;
            height: 400px;
            padding: 10px 50px 20px 50px;
            margin: 0px 50px 10px 50px;
        }

        #bioHeader{
            position:relative;
            left: 650px;
            bottom: 420px;

        }

        #courseInfo{
            position:relative;
            bottom: 200px;
            left: 75px;
        }

        label {
            font-size:25px;
            position:relative;

        }

        #submit{
            position: relative;
            top: 100px;
            left: 125px;
        }

        button{
            position: relative;
            bottom: 500px;
            left: 222px;
        }

        td{
            text-align: center;
        }

        .error {color: red;}
  	</style>

    <script type="text/javascript">
      function checkInputs(event){
        var frmObject = document.forms[0];
        var output = "Please correct the errors on the page.";
        var checkError = false;

        //Full name text box
        if (frmObject.searchBox.value==""){
          checkError = true;
          document.getElementsByTagName("input")[0].style.color = "#ff0000";
          var z = document.getElementById("search");
          var para = document.createElement("div");
          para.className = "error";  //add a class name to the new div element
          var t = document.createTextNode("Please enter a value in the textBox");
          para.appendChild(t);
          z.appendChild(para);
        }

        if(checkError){
          alert(output);
          event.preventDefault();
          return;
        }
      }

      function init(){
		  var x = document.getElementsByTagName("form")[0];
		  x.addEventListener("submit", checkInputs);
	  }

      document.addEventListener("DOMContentLoaded", init);
      }
  	</script>
    <body>

      <div class="sidenav">
        <img class="logo" src="sslogo.png" width="125px">
        <ul class="nav nav-pills">
          <li><a href="Homepage.php">Home</a></li>
          <li class="active"><a href="SessionPage.php">Session</a></li>
          <li><a href="SearchInactiveSessionPage.php">Search</a></li>
          <li><a href="AccountManagement.php">Profile</a></li>
        </ul>
      </div>
        <div class="content">

            <div id="searchInfo">

                <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

                    <label>Search & Filter Sessions</label>
                    </br>
                    <select name = "searchType">
                        <option name=default value = "AllSessions">All Sessions</option>
                        <option name=searchSubject value = "CRN" <?php if (isset($_GET['submit']) && $_GET['searchType'] == 'CourseSubject') echo 'selected="selected"'; ?> >CRN</option>
                        <option name=searchCNum value = "Course Number" <?php if (isset($_GET['submit']) && $_GET['searchType'] == 'CourseNumber') echo 'selected="selected"'; ?> >Course Number</option>
                        <option name=searchCRN value = "Course Subject" <?php if (isset($_GET['submit']) && $_GET['searchType'] == 'CRN') echo 'selected="selected"'; ?> >Course Subject</option>
                    </select>
                    <input type="text" id="search" name="searchBox" id="searchBox">
                    <input type="submit" name="submit" value="Search">
                    <?php

                    ?>

                </form>

            </div>

            <h2 id="bioHeader">My Courses</h2>
            <div name="myCourses" id="userCourses">

                <table id="myClassInfo" border=1 width=800px>
                    <tr>
                        <th>CRN</th>
                        <th>Subject</th>
                        <th>Course Number</th>
                        <th>Session Date</th>
                        <th>Start Time</th>
                        <th>Location</th>
                        <th>Remove?</th>
                    </tr>
                    <?php

                        require_once("db.php");
                        $sql = "select s.courseCRN, c.courseSubject, c.courseNum, s.sessionDate, s.sessionST, s.sessionLocation
                                from usersession u, sessions s, course c
                                where u.userID = '4' AND u.sessionID = s.sessionID AND c.courseCRN = s.courseCRN";
                        $result = $mydb->query($sql);
                        while($row=mysqli_fetch_array($result)){

                            echo "<tr>";
                            echo "<td>".$row['courseCRN']."</td><td> ".$row['courseSubject']."</td><td>".$row['courseNum']."</td><td>".$row['sessionDate']."</td><td>".$row['sessionST']."</td><td>".$row['sessionLocation']."</td><td>".
                            "<form method='post' action=".$_SERVER['PHP_SELF']."><input type=submit name=sessionRemove value='Remove Session'><input type='hidden' name='ROW' value=".$row['courseCRN']."></form>";
                            echo "</tr>";

                        }

                    ?>
                </table>

    </body>

</html>
