<!doctype html>
<html

    <head>

        <title>Template for the sidebar</title>
        <meta name="author" content="Edward Reed">
        <link rel="stylesheet" type="text/css" href="studyspaces.css">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <script src="js/bootstrap.min.js"></script>
    </head>


    <style>

    #overview {
      position: absolute;
      width: 327px;
      height: 55px;
      left: 517px;
      top: 78px;

      background: #C4C4C4;
      box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
      border-radius: 10px;

    }

        #timer {
          position: absolute;
          width: 346px;
          height: 346px;
          left: 498px;
          top: 177px;
          border-radius: 50%;

          background: #C4C4C4;
        }

        #end {
          position: absolute;
          width: 327px;
          height: 55px;
          left: 517px;
          top: 565px;
          background: #C4C4C4;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
        }

        input[name = "joinSession"] {
          position: absolute;
          width: 327px;
          height: 55px;
          left: -2px;
          top: 0px;
          font-size: 24px;
          font-weight: bold;
          font-family: roboto;
          background: #C4C4C4;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
          border-radius: 10px;
        }

    </style>

    <script src="jquery-3.1.1.min.js"></script>


    <body>
        <?php
          session_start();
          echo "<p>".$_SESSION["sessionID"]."</p>";
        ?>
        <div class="sidenav">
          <img class="logo" src="sslogo.png" width="125px">
          <ul class="nav nav-pills">
            <li><a href="Homepage.php">Home</a></li>
            <li class="active"><a href="SessionPage.php">Session</a></li>
            <li><a href="SearchInactiveSessionPage.php">Search</a></li>
            <li><a href="AccountManagement.php">Profile</a></li>
          </ul>
        </div>
        <div class="content">

            <div id="overview">
                 <form method="post" action="sessionOverview.php" name="overview"><input type=submit name=joinSession value='Session Overview'>
                 <p style="position: absolute; width: 552px; height: 57px; left: 0px; top: -80px; font-family: roboto; font-style: normal; font-weight: bold; font-size: 24px; line-height: 28px; display: flex; align-items: center; text-align: center;"> Countdown until session begins </p>

            </div>

            <div id="timer">

            </div>

            <p id = "clock" style = "position: absolute; width: 317px; height: 122px; left: 530px; top: 290px; font-family: roboto; font-style: normal; font-weight: 900; font-size: 72px; line-height: 84px; display: flex; align-items: center; text-align: center;"> 00:00:00 </p>

            </p>

        </div>

    </body>

    <script>
      $(function(){
        setInterval(updateTime, 1000);

        function updateTime(){
          var currentDate = new Date();
          var eventDate = new Date("December 25, 2019 18:30 EST");
          var timeDiff = (eventDate - currentDate)/1000;
          var Days = Math.floor(timeDiff / 86400);
          var Hours = Math.floor((timeDiff%86400) / 3600);
          var Minutes = Math.floor((timeDiff%86400%3600) / 60);
          var Seconds = Math.floor(timeDiff%86400%3600%60);
          if (Minutes >= 10) {
            if (Seconds >= 10) {
                var output = Hours + ":" + Minutes + ':' + Seconds;
            }
            else {
                var output = Hours + ":" + Minutes + ':0' + Seconds;
            }
          }
          else {
            if (Seconds >= 10) {
                var output = Hours + ":0" + Minutes + ':' + Seconds;
            }
            else {
                var output = Hours + ":0" + Minutes + ':0' + Seconds;
            }

          }

          $("#clock").text(output);
          $("#timer").css({"font-size": "72px"});
          $("#timer").css({"position": "absolute"});
          $("#clock").css({"left": "550px"});
          $("#clock").css({"top": "290px"});
        }
      })

    </script>
</html>
